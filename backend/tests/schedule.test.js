/*
This should be the test for Schedule.js

ScheduleData.js is constructor class and data structure for storing jadwal ujian purpose
For now its 2.30 AM GMT+7 and my house was under blackout. so leave it alone for while

@author = Muhamad Nicky Salim
*/
const Schedule = require("../src/algorithm/Schedule.js");

describe('Data Structure', () => {
    it("Checking if constructor is working right", ()=> {
        var testInputSessions = 3;
        var testInputDays = 5;
        var result = new Schedule(testInputSessions, testInputDays);
        expect(result.maxSessionsPerDay == testInputSessions);
        expect(result.days == testInputDays);
        expect(result.valid == true);

        testInputSessions = -1;
        testInputDays = 5;
        result = new Schedule(testInputSessions, testInputDays);
        expect(result.maxSessionsPerDay == testInputSessions);
        expect(result.days == testInputDays);
        expect(result.valid == false);

        testInputSessions = 3;
        testInputDays = 0;
        result = new Schedule(testInputSessions, testInputDays);
        expect(result.maxSessionsPerDay == testInputSessions);
        expect(result.days == testInputDays);
        expect(result.valid == false);
    })

    it("Checking if inserting courses is right", ()=> {
        //valid case
        var testInput = [
            ["ECAU601104", "Pengantar Akuntansi" , 3, "Wajib Fakultas", "IE", 1, 1, 1, "Senin 1", 1, 12, 1],
            ["ECAU601201", "Akuntansi Keuangan 1", 3, "Wajib Program Studi", "EMA", 2, 2, 2, "Senin 2", 2, 6, 2]
        ];
        var testMaxSessionsPerDay = 2;
        var testDays = 3;

        var testObject = new Schedule(testMaxSessionsPerDay, testDays);
        testObject.insertCourses(testInput);
        var result = testObject.courses;

        expect(testInput.length == result.length);

        //invalid case
        testInput = [
            ["ECAU601104", "Pengantar Akuntansi" , 3, "Wajib Fakultas", "IE", 1, 1, 1, "Senin 1", 1, 12, 1],
            ["ECAU601201", "Akuntansi Keuangan 1", 3, "Wajib Program Studi", "EMA", 2, 2, 2, "Senin 2", 2, 6, 2]
        ];
        testMaxSessionsPerDay = 0;
        testDays = 3;

        testObject = new Schedule(testMaxSessionsPerDay, testDays);
        testObject.insertCourses(testInput);
        result = testObject.courses;

        expect(result == undefined);
    })

    it("Checking if inserting room is right", ()=> {
        //valid case
        var testInput = [
            [1, "A.101", "Flat", 80, 0.6, 48],
            [11, "A.111", "Theatre", 45, 0.5, 23]
        ];

        var testMaxSessionsPerDay = 3;
        var testDays = 7;

        var testObject = new Schedule(testMaxSessionsPerDay, testDays);
        testObject.insertRooms(testInput);
        var result = testObject.rooms;

        expect(testInput.length == result.length);
        expect(result[0].length == 3); //room name, class type, exam capacity
        expect(result[1] == ["A.111", "Theatre", 23]);

        //invalid case
        testInput = [
            [1, "A.101", "Flat", 80, 0.6, 48],
            [11, "A.111", "Theatre", 45, 0.5, 23]
        ];

        testMaxSessionsPerDay = 3;
        testDays = -2;

        testObject = new Schedule(testMaxSessionsPerDay, testDays);
        testObject.insertRooms(testInput);
        result = testObject.rooms;

        expect(result == undefined);
    })

    it("Checking if table is right", ()=> {
        //valid case
        var testInputCourses = [
            ["ECAU601104", "Pengantar Akuntansi" , 3, "Wajib Fakultas", "IE", 1, 1, 1, "Senin 1", 1, 12, 1],
            ["ECAU601201", "Akuntansi Keuangan 1", 3, "Wajib Program Studi", "EMA", 2, 2, 2, "Senin 2", 2, 6, 2]
        ];
        var testInputRooms = [
            [1, "A.101", "Flat", 80, 0.6, 48],
            [11, "A.111", "Theatre", 45, 0.5, 23]
        ];
        var testMaxSessionsPerDay = 2;
        var testDays = 3;

        var testObject = new Schedule(testMaxSessionsPerDay, testDays);
        testObject.insertCourses(testInputCourses);
        testObject.insertRooms(testInputRooms);
        testObject.createTable();

        var result = testObject.table;
        var expected = [
            // [days, sessions, [room, capacity course]]
            [1, 1, ["A.101", 48, null]],
            [1, 1, ["A.111", 23, null]],
            [1, 2, ["A.101", 48, null]],
            [1, 2, ["A.111", 23, null]],
            [2, 1, ["A.101", 48, null]],
            [2, 1, ["A.111", 23, null]],
            [2, 2, ["A.101", 48, null]],
            [2, 2, ["A.111", 23, null]],
            [3, 1, ["A.101", 48, null]],
            [3, 1, ["A.111", 23, null]],
            [3, 2, ["A.101", 48, null]],
            [3, 2, ["A.111", 23, null]],
        ];
        expect(result == expected);

        //invalid case
        testInputCourses = [
            ["ECAU601104", "Pengantar Akuntansi" , 3, "Wajib Fakultas", "IE", 1, 1, 1, "Senin 1", 1, 12, 1],
            ["ECAU601201", "Akuntansi Keuangan 1", 3, "Wajib Program Studi", "EMA", 2, 2, 2, "Senin 2", 2, 6, 2]
        ];
        testInputRooms = [
            [1, "A.101", "Flat", 80, 0.6, 48],
            [11, "A.111", "Theatre", 45, 0.5, 23]
        ];
        testMaxSessionsPerDay = -1;
        testDays = 3;

        testObject = new Schedule(testMaxSessionsPerDay, testDays);
        testObject.insertCourses(testInputCourses);
        testObject.insertRooms(testInputRooms);
        testObject.createTable();

        result = testObject.table;
        expect(result == undefined);
    })

    it("Data preparations checking, combine of all data insertion and init schedule", ()=> {
        // valid case
        var testInputCourses = [
            ["ECAU601104", "Pengantar Akuntansi" , 3, "Wajib Fakultas", "IE", 1, 1, 1, "Senin 1", 1, 12, 1],
            ["ECAU601201", "Akuntansi Keuangan 1", 3, "Wajib Program Studi", "EMA", 2, 2, 2, "Senin 2", 2, 6, 2]
        ];
        var testInputRooms = [
            [1, "A.101", "Flat", 80, 0.6, 48],
            [11, "A.111", "Theatre", 45, 0.5, 23]
        ];
        var testMaxSessionsPerDay = 2;
        var testDays = 3;

        var testObject = new Schedule(testMaxSessionsPerDay, testDays);
        var result = testObject.dataPreparations(testInputCourses, testInputRooms);

        expect(result == true);

        //invalid case
        testInputCourses = [
            ["ECAU601104", "Pengantar Akuntansi" , 3, "Wajib Fakultas", "IE", 1, 1, 1, "Senin 1", 1, 12, 1],
            ["ECAU601201", "Akuntansi Keuangan 1", 3, "Wajib Program Studi", "EMA", 2, 2, 2, "Senin 2", 2, 6, 2]
        ];
        testInputRooms = [
            [1, "A.101", "Flat", 80, 0.6, 48],
            [11, "A.111", "Theatre", 45, 0.5, 23]
        ];
        var testMaxSessionsPerDay = -1;
        var testDays = 3;

        var testObject = new Schedule(testMaxSessionsPerDay, testDays);
        var result = testObject.dataPreparations(testInputCourses, testInputRooms);

        expect(result == false);
    })
})


