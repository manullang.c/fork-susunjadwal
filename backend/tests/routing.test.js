const request = require('supertest');
const app = require('../app');
const fs = require('fs');
describe('GET /', function () {
  it('responds with a json file', () => {
    return request(app)
      .get('/')
      .expect('Content-Type', /json/)
      .expect(200);
  });

  it('responds with something that has "Hello world" as its message.', () => {
    return request(app)
      .get('/')
      .expect((res) => {
        res.body = "Hello World"
      })
      .expect(200);
  });
});

describe('GET /', ()=> {
  it('returns 404', (done) => {
    return request(app).get('/not-a-link').expect(404,done);
  }, 30000)
});