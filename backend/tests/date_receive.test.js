const request = require('supertest');
const app = require('../app');

describe('POST /send_date', function () {
    it('responds the date input', (done) => {
        request(app)
            .post('/send_date')
            .send({
                dates: [
                    {
                        day: 15,
                        month: "Mar",
                        year: 2020
                    }, {
                        day: 17,
                        month: "Mar",
                        year: 2020
                    }
                ],
                sessionsPerDate: 2
            })
            .expect(200)
            
            .then(response => {
                expect(response.body.dates[0].day).toBe(15);
                expect(response.body.dates[0].month).toBe("Mar");
                expect(response.body.dates[0].year).toBe(2020);
                expect(response.body.sessionsPerDate).toBe(2);
                expect(response.body.dates.length).toBe(2);
                done();
            });
    });
});