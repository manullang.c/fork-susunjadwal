const formatValidator = require("../src/format_validator.js");
const xlsx = require('xlsx');

const scheduleWB = xlsx.readFile('./tests/file_testing/jadwal_kuliah.xlsx');
const roomWB = xlsx.readFile('./tests/file_testing/Ruangan_ujian.xlsx');

function get_data(wb) {
    var sheetName = wb.SheetNames;
    var data = wb.Sheets[sheetName[0]];
    return data;
}

const scheduleArray = xlsx.utils.sheet_to_json(get_data(scheduleWB),{header:1,defval:""});
const roomArray = xlsx.utils.sheet_to_json(get_data(roomWB), {header:1,defval:""});

console.log(scheduleArray[0]);
console.log(roomArray[0]);

describe('format validator', () => {
    it('validate ruang ujian', () => {
        // this one for fail case
        var testResult = formatValidator.validateRoom(scheduleArray);
        var expectedResult = false;
        expect(testResult).toEqual(expectedResult);

        // this one for success case
        testResult = formatValidator.validateRoom(roomArray);
        expectedResult = true;
        expect(testResult).toEqual(expectedResult);
    });

    it('validate jadwal kuliah', () => {
        // this one for fail case
        var testResult = formatValidator.validateSchedule(roomArray);
        var expectedResult = false;
        expect(testResult).toEqual(expectedResult);

        // this one for success case
        testResult = formatValidator.validateSchedule(scheduleArray);
        expectedResult = true;
        expect(testResult).toEqual(expectedResult);
    });
});