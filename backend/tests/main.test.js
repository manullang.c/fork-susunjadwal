const main = require("../src/algorithm/Main.js");
const xlsx = require("xlsx");
const fs = require("fs");

describe('parsing file', ()=>{
    it('is working', ()=>{
        var buff = fs.readFileSync('./tests/file_testing/test.xlsx');
        var wb = xlsx.read(buff, {type:"buffer"});
        var ws = wb.Sheets[wb.SheetNames[0]];
        var expectedJSON = xlsx.utils.sheet_to_json(ws);
        var expectedArray = xlsx.utils.sheet_to_json(ws,{header:1,defval:""});
        expect(main.parse(buff).jsondata).toEqual(expectedJSON);
        expect(main.parse(buff).arraydata).toEqual(expectedArray);
    })
});