const request = require('supertest');
const fs = require('fs');
const app = require('../app');

describe('get table',()=>{
    it('is working',(done)=>{
        fs.copyFileSync('./tests/file_testing/jadwal_kuliah.xlsx', './files/schedule.xlsx');
        fs.copyFileSync('./tests/file_testing/Ruangan_ujian.xlsx', './files/room.xlsx');
        request(app)
            .post('/get_table')
            .send({dates: ['4']})
            .expect(200)
            .then((res)=>{
                // expect(res.body.message.schedule.hasNull).toBeFalsy();
                // expect(res.body.message.room.hasNull).toBeFalsy();
                done()
            });
    },30000);
    it('rejects',(done)=>{
        fs.copyFileSync('./tests/file_testing/jadwal_kuliah.xlsx', './files/room.xlsx');
        fs.copyFileSync('./tests/file_testing/Ruangan_ujian.xlsx', './files/schedule.xlsx');
        request(app)
            .post('/get_table', ['2'])
            .expect(200)
            .then((res)=>{
                // expect(res.body.message).toEqual('files are not valid');
                done()
            });
    },30000);
});