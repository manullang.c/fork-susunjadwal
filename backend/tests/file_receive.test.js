const request = require('supertest');
const app = require('../app');
const fs = require('fs');

describe('POST /upload succeeds to', ()=> {
  it('create a schedule file after input is posted', (done) => {
    request(app)
      .post('/upload/schedule')
      .attach('file', './tests/file_testing/jadwal_kuliah.xlsx')
      .expect(200)
      .then((res)=>{
        expect(res.body.message).toEqual('Schedule file uploaded');
        expect(fs.existsSync('./files/schedule.xlsx')).toBeTruthy();
        done();
      });
  },30000);
  it('create a room file after input is posted', (done) => {
    request(app)
      .post('/upload/room')
      .attach('file', './tests/file_testing/Ruangan_ujian.xlsx')
      .expect(200)
      .then((res)=>{
        expect(res.body.message).toEqual('Room file uploaded');
        expect(fs.existsSync('./files/room.xlsx')).toBeTruthy();
        done();
      })
  },30000)
});

describe('POST /upload', ()=>{
  it('throws error on upload/schedule', (done)=>{
    request(app)
      .post('/upload/schedule')
      .attach('file', './tests/file_testing/a.txt')
      .expect(500)
      .then((err)=>{
        expect(err.text).toMatch('File format must be in xls or xlsx')
        done();
      });
  },30000);
  it('throws error on upload/room', (done)=>{
    request(app)
      .post('/upload/room')
      .attach('file', './tests/file_testing/a.txt')
      .expect(500)
      .then((err)=>{
        expect(err.text).toMatch('File format must be in xls or xlsx')
        done();
      });
  },30000);
})