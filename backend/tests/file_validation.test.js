const validate = require('../src/file_validation.js');
const xlsx = require('xlsx');

var testFile = xlsx.readFile('./tests/file_testing/testFile.xlsx');
var testFileWithHole = xlsx.readFile('./tests/file_testing/testFile_withHole.xlsx');
var scheduleWB = xlsx.readFile('./tests/file_testing/jadwal_kuliah.xlsx');
var roomWB = xlsx.readFile('./tests/file_testing/Ruangan_ujian.xlsx');

function get_sheet(wb) {
    var sheetName = wb.SheetNames;
    var data = wb.Sheets[sheetName[0]];
    return data;
}

var arrayData = xlsx.utils.sheet_to_json(get_sheet(testFile),{header:1,defval:""});
var arrayDataWithHole = xlsx.utils.sheet_to_json(get_sheet(testFileWithHole),{header:1,defval:""});
var scheduleArray = xlsx.utils.sheet_to_json(get_sheet(scheduleWB),{header:1,defval:""});
var roomArray = xlsx.utils.sheet_to_json(get_sheet(roomWB),{header:1,defval:""});
describe('file validation', ()=>{
    it('returns a statement about complete data', ()=>{
        console.log(validate.checkForHole(arrayData).nullData);
        expect(validate.checkForHole(arrayData).hasNull).toBeFalsy();
        expect(validate.checkForHole(arrayData).nullData).toEqual([]);
    })
    it('returns an argument of missing cells', ()=>{
        console.log(validate.checkForHole(arrayDataWithHole).nullData);
        expect(validate.checkForHole(arrayDataWithHole)).toBeTruthy();
        expect(validate.checkForHole(arrayDataWithHole).nullData).toEqual(['B3']);
    })
});

describe('test file', ()=>{
    it('logs',()=>{
        console.log(validate.checkForHole(scheduleArray).nullData);
        console.log(validate.checkForHole(roomArray).nullData);
    })
})

describe('number zero', ()=>{
    it('is not the spreadsheet column index',()=>{
        expect(validate.numToSSColumn(0)).toBeUndefined();
    })
});