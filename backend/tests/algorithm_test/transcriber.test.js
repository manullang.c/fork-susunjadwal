const Transcriber = require('../../src/algorithm/graph_coloring/Transcriber');
const CustomGraphColoring = require('../../src/algorithm/graph_coloring/CustomGraphColoring');
const Constraint = require('../../src/algorithm/graph_coloring/Constraint');



let data = [
    [],
    ['TEST0', 'Matkul Uji 1', null, null, 'IE', null, null, null, 'Senin 1', 1, 4, 2],
    ['TEST1', 'Matkul Uji 2', null, null, 'EAK', null, null, null, 'Selasa 1', 1, 4, 1],
    ['TEST2', 'Should not appear', null, null, 'ALL', null, null, null, 'Selasa 1', 1, 4, 1],
    ['TEST3', 'Matkul Uji 3', null, null, 'FEB', null, null, null, 'Selasa 1', 1, 4, 3],
    ['']
];

let rooms = [
    [],
    [0, 'Ruang uji 1', null, null, null, 5],
    [0, 'Ruang uji 2', null, null, null, 10],
    [0, 'Ruang uji 3', null, null, null, 15],
    [0, 'Ruang uji 4', null, null, null, 20],
    [0, 'Ruang uji 4', null, null, null, 20],
    [0, 'Ruang uji 4', null, null, null, 20]
];

let dayList = [

    { day: 28, month: 'Mar', year: 2012 },
    { day: 29, month: 'Mar', year: 2012 },
    { day: 30, month: 'Mar', year: 2012 },
    { day: 31, month: 'Mar', year: 2012 },
    { day: 1, month: 'Apr', year: 2012 },
    { day: 2, month: 'Apr', year: 2012 },
    { day: 3, month: 'Apr', year: 2012 },
    { day: 4, month: 'Apr', year: 2012 },
    { day: 5, month: 'Apr', year: 2012 },
    { day: 6, month: 'Apr', year: 2012 },

]
describe('transcriber collect functionality', () => {
    it('puts their subjects to their respective constraint variable', () => {
        let transcriber = new Transcriber(data, rooms, dayList, () => { }, 1);
        expect(transcriber.bucket.get('Senin 1 IE')).toContain('TEST0');
        expect(transcriber.bucket.get('Selasa 1 EAK')).toContain('TEST3');
    });

    it('doesnt put ALL subjects anywhere', () => {
        let transcriber = new Transcriber(data, rooms, dayList, () => { }, 1);
        for (const variable of transcriber.bucket) {
            expect(variable).not.toContain('TEST2');
        }
    });

    it('puts FEB subjects on all variables on the same date.', () => {
        let transcriber = new Transcriber(data, rooms, dayList, () => { }, 1);
        expect(transcriber.bucket.get('Selasa 1 IE')).toContain('TEST3');
        expect(transcriber.bucket.get('Selasa 1 EAK')).toContain('TEST3');
    });
})

describe('transcriber-graph coloring interaction', () => {
    it('creates and calls the graph coloring algorithm', () => {
        let transcriber = new Transcriber(data, rooms, dayList, () => { }, 2);
        let result = transcriber.runAlgorithm();
        expect(result.output).toContainEqual(['undefined, 28 Mar', 1, ['Ruang uji 4', 0, 'Matkul Uji 1']]);
    })
})

describe('transcriber output functionality', () => {
    it('can be successful', () => {
        let transcriber = new Transcriber([
            [],
            ['TEST0', 'Matkul Uji 1', null, null, 'IE', null, null, null, 'Senin 1', 1, 4, 2],
            ['TEST2', 'Matkul Uji 4', null, null, 'IE', null, null, null, 'Senin 1', 1, 4, 2],
            ['TEST3', 'Matkul Uji 4', null, null, 'IE', null, null, null, 'Senin 1', 1, 4, 2],
            ['TEST1', 'Matkul Uji 2', null, null, 'EAK', null, null, null, 'Selasa 1', 1, 4, 1],
            ['TEST3', 'Matkul Uji 3', null, null, 'FEB', null, null, null, 'Selasa 1', 1, 4, 3],
            ['']
        ], rooms, dayList.slice(0, 2), () => { }, 2);
        let result = transcriber.runAlgorithm();
        expect(result.success).toBe(true);
    })

    it('can be failing on tight constraint', () => {
        let transcriber = new Transcriber([
            [],
            ['TEST0', 'Matkul Uji 1', null, null, 'FEB', null, null, null, 'Senin 1', 1, 4, 2],
            ['TEST2', 'Matkul Uji 4', null, null, 'FEB', null, null, null, 'Senin 1', 1, 4, 2],
            ['TEST3', 'Matkul Uji 3', null, null, 'FEB', null, null, null, 'Senin 1', 1, 4, 3],
            ['TEST4', 'Matkul Uji 3', null, null, 'FEB', null, null, null, 'Senin 1', 1, 4, 3],
            ['TEST5', 'Matkul Uji 3', null, null, 'FEB', null, null, null, 'Senin 1', 1, 4, 3],
            ['']
        ], rooms, dayList.slice(0, 1), () => { }, 2);
        let result = transcriber.runAlgorithm();

        expect(result.success).toBe(false);
        expect(result.reason).toBe('sistem tidak dapat menemukan susunan jadwal');
    })

    it('can be failing on tight timing', () => {
        let transcriber = new Transcriber([
            [],
            ['TEST0', 'Matkul Uji 1', null, null, 'FEB', null, null, null, 'Senin 1', 1, 4, 1],
            ['TEST2', 'Matkul Uji 4', null, null, 'FEB', null, null, null, 'Senin 2', 1, 4, 1],
            ['TEST3', 'Matkul Uji 3', null, null, 'FEB', null, null, null, 'Senin 3', 1, 4, 1],
            ['']
        ], rooms.slice(0, 1), dayList, () => { }, 1);
        let result = transcriber.runAlgorithm();
        expect(result.success).toBe(false);
        expect(result.reason).toBe('sistem dapat menemukan susunan jadwal, tetapi tidak dapat menemukan susunan ruangan');
    })
})

let constraintArray = [ // There's a solution
    [false, false, false, false, false, true, false],    // 0 - 1, 5
    [false, false, true, false, false, true, false],     // 1 - 0, 2, 5
    [false, true, false, true, false, true, false],     // 2 - 1, 3, 5
    [false, false, true, false, true, true, false],     // 3 - 2, 4, 5
    [false, false, false, true, false, true, false],    // 4 - 3, 5
    [true, true, true, true, true, false, false],       // 5 - 0, 1, 2, 3, 4
    [false, false, false, false, false, false, false]   // 6 -
];

let constraintArray2 = [ // There's no solution
    [false, true, false, false, false, true, false],    // 0 - 1, 5
    [true, false, true, false, false, true, true],     // 1 - 0, 2, 5, 6
    [false, true, false, true, false, true, true],     // 2 - 1, 3, 5, 6
    [false, false, true, false, true, true, false],     // 3 - 2, 4, 5
    [false, false, false, true, false, true, false],    // 4 - 3, 5
    [true, true, true, true, true, false, true],       // 5 - 0, 1, 2, 3, 4, 6
    [false, true, true, false, false, true, false]   // 6 - 1, 2, 5
];

describe('custom graph coloring functionality', () => {
    it('works on success', () => {
        let constraint = new Constraint(constraintArray);
        let gc = new CustomGraphColoring(
            constraint,
            3,
            () => { },
            () => { },
            [
                { maxCapacity: 3 },
                { maxCapacity: 0 },
                { maxCapacity: 3 },
                { maxCapacity: 1 },
                { maxCapacity: 2 },
                { maxCapacity: 3 },
                { maxCapacity: 2 },
                { maxCapacity: 0 }
            ])
        gc.runBacktrack();
    })

    it('works on fail', () => {
        let constraint = new Constraint(constraintArray2);
        let success = false;
        let gc = new CustomGraphColoring(
            constraint,
            3,
            (s, r) => { success = s },
            () => { },
            [
                { maxCapacity: 3 },
                { maxCapacity: 2 },
                { maxCapacity: 1 },
                { maxCapacity: 3 },
                { maxCapacity: 2 },
                { maxCapacity: 3 },
                { maxCapacity: 0 },
                { maxCapacity: 1 }
            ])
        gc.runBacktrack();
        expect(success).toBe(false);
    })
})