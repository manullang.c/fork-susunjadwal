const State = require('../../src/algorithm/graph_coloring/State');
const Constraint = require('../../src/algorithm/graph_coloring/Constraint');

let constraint = new Constraint([
    [false, true, false, false, false, true, false],    // 0 - 1, 5
    [true, false, true, false, false, true, false],     // 1 - 0, 2, 5
    [false, true, false, true, false, true, false],     // 2 - 1, 3, 5
    [false, false, true, false, true, true, false],     // 3 - 2, 4, 5
    [false, false, false, true, false, true, false],    // 4 - 3, 5
    [true, true, true, true, true, false, false],       // 5 - 0, 1, 2, 3, 4
    [false, false, false, false, false, false, false]   // 6 -
]);

describe('the state generator', () => {
    it('generates the right state', () => {
        let state = State.fromDomainSize(constraint, 3)
        expect(state.constraint).toBe(constraint);
        expect(state.array[0]).toContain(0);
        expect(state.array[0]).toContain(1);
        expect(state.array[0]).toContain(2);
        expect(state.array[0]).not.toContain(3);
    })
})