const GraphColoring = require('../../src/algorithm/graph_coloring/GraphColoring');
const Constraint = require('../../src/algorithm/graph_coloring/Constraint');
const State = require('../../src/algorithm/graph_coloring/State');

let constraintArray = [ // There's a solution
    [false, true, false, false, false, true, false],    // 0 - 1, 5
    [true, false, true, false, false, true, false],     // 1 - 0, 2, 5
    [false, true, false, true, false, true, false],     // 2 - 1, 3, 5
    [false, false, true, false, true, true, false],     // 3 - 2, 4, 5
    [false, false, false, true, false, true, false],    // 4 - 3, 5
    [true, true, true, true, true, false, false],       // 5 - 0, 1, 2, 3, 4
    [false, false, false, false, false, false, false]   // 6 -
];

let constraintArray2 = [ // There's no solution
    [false, true, false, false, false, true, false],    // 0 - 1, 5
    [true, false, true, false, false, true, true],     // 1 - 0, 2, 5, 6
    [false, true, false, true, false, true, true],     // 2 - 1, 3, 5, 6
    [false, false, true, false, true, true, false],     // 3 - 2, 4, 5
    [false, false, false, true, false, true, false],    // 4 - 3, 5
    [true, true, true, true, true, false, true],       // 5 - 0, 1, 2, 3, 4, 6
    [false, true, true, false, false, true, false]   // 6 - 1, 2, 5
];

function successRecorder() {
    this.success = false;
    /** @type {State} */
    this.state = null;
    this.record = (success, state) => {
        this.success = success;
        this.state = state;
    }
}
describe('the graph coloring algorithm', () => {
    it('does the algorithm correctly if the solution is found', () => {
        let constr = new Constraint(constraintArray);
        let recorder = new successRecorder();
        let gc = new GraphColoring(constr, 3, recorder.record, (progress) => {});
        gc.runBacktrack();
        expect(recorder.success).toBe(true);
        for (let assignment of recorder.state.array) {
            expect(assignment.size).toBe(1);
        }
        for (let [index1, assignment1] of recorder.state.array.entries()) {
            for (let [index2, assignment2] of recorder.state.array.entries()) {
                let value1 = assignment1.values().next().value;
                let value2 = assignment2.values().next().value;
                expect(constr.validValueSet(index1, value1, index2, value2)).toBe(true);
            }
        }
    })

    it('fails algorithm correctly if the solution is not found', () => {
        let constr = new Constraint(constraintArray2);
        let recorder = new successRecorder();
        let gc = new GraphColoring(constr, 3, recorder.record, (progress) => {});
        gc.runBacktrack();
        expect(recorder.success).toBe(false);
    })
})