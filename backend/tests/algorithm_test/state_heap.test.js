const StateHeap = require('../../src/algorithm/graph_coloring/StateHeap');
const State = require('../../src/algorithm/graph_coloring/State');

jest.mock('../../src/algorithm/graph_coloring/State');

/**
 * Alternative implementation of State for testing. The constructor instead
 * takes the argument of num, which is the valid amount of state.
 */
State.mockImplementation((num) => {
    return new function() {
        this.count = num;
        this.countValidValue = function () {
            return this.count;
        }
    } ();
})

describe('StateHeap implementation', () => {
    it('functions as a min heap.', () => {
        heap = new StateHeap();
        heap.push(new State(6));
        heap.push(new State(3));
        heap.push(new State(4));
        heap.push(new State(2));
        heap.push(new State(4));
        heap.push(new State(9));
        heap.push(new State(6));
        expect(heap.pop().countValidValue()).toBe(2);
        expect(heap.pop().countValidValue()).toBe(3);
        expect(heap.pop().countValidValue()).toBe(4);
        expect(heap.pop().countValidValue()).toBe(4);
        expect(heap.pop().countValidValue()).toBe(6);
        expect(heap.pop().countValidValue()).toBe(6);
        expect(heap.pop().countValidValue()).toBe(9);
    })

    it('will exhaust if heap is empty', () => {
        heap = new StateHeap();
        heap.push(new State(0));
        expect(heap.pop().countValidValue()).toBe(0);
        expect(heap.pop()).toBeUndefined();
    })

    it('has its `isEmpty()` true if the heap is empty', () => {
        heap = new StateHeap();
        expect(heap.isEmpty()).toBe(true);
        heap.push(new State(0));
        expect(heap.pop().countValidValue()).toBe(0);
        expect(heap.isEmpty()).toBe(true);
    })

    it('has its `isEmpty()` false if the heap is not empty', () => {
        heap = new StateHeap();
        heap.push(new State(0));
        expect(heap.isEmpty()).toBe(false);
    })
})