const Constraint = require('../../src/algorithm/graph_coloring/Constraint');

console.log(Constraint)

let constraintArray = [
    [false, true, false, false, false, true, false],    // 0 - 1, 5
    [true, false, true, false, false, true, false],     // 1 - 0, 2, 5
    [false, true, false, true, false, true, false],     // 2 - 1, 3, 5
    [false, false, true, false, true, true, false],     // 3 - 2, 4, 5
    [false, false, false, true, false, true, false],    // 4 - 3, 5
    [true, true, true, true, true, false, false],       // 5 - 0, 1, 2, 3, 4
    [false, false, false, false, false, false, false]   // 6 -
];
describe('Constraint state functionality', () => {
    it('successfully created a Constraint object', () => {
        new Constraint(constraintArray);
    });

    it('has the correct node count', () => {
        let constraint = new Constraint(constraintArray);
        expect(constraint.size).toBe(7);
    });

    test('for various inclusions', () => {
        let constraint = new Constraint(constraintArray);
        expect(constraint.includes(0, 0)).toBe(false);
        expect(constraint.includes(1, 0)).toBe(true);
        expect(constraint.includes(2, 0)).toBe(false);
    });

    test('for the functionalities of `iterateOverNodes`', () => {
        let constraint = new Constraint(constraintArray);
        let generator = constraint.iterateOverNodes(1);
        expect(generator.next().value).toBe(0);
        expect(generator.next().value).toBe(2);
        expect(generator.next().value).toBe(5);
    });

    test('for the functionalities of `iterateOverNodes`, with exclusion', () => {
        let constraint = new Constraint(constraintArray);
        let generator = constraint.iterateOverNodes(1, 2);
        expect(generator.next().value).toBe(0);
        expect(generator.next().value).toBe(5);
    });

    test('for `mostConstrainingNode`', () => {
        let constraint = new Constraint(constraintArray);
        let node = constraint.mostConstrainingNode(
            [0, 1, 2, 3, 4, 5, 6]
        )
        expect(node).toBe(5)
    })

    test('for `validValueSet`', () => {
        let constraint = new Constraint(constraintArray);
        expect(constraint.validValueSet(1, 4, 2, 4)).toBe(false);
        expect(constraint.validValueSet(1, 4, 2, 3)).toBe(true);
        expect(constraint.validValueSet(0, 4, 2, 3)).toBe(true);
        expect(constraint.validValueSet(0, 4, 2, 4)).toBe(true);
    })
});