class StateHeap {
    /**
     * Creates an empty min heap of state, to be filled when calculating
     * the least constrained variable.
     */
    constructor() {
        /** @type {State[]} */
        this.array = [];
    }
    /**
     * Push the state to the heap.
     *
     * @param {State} state - the state to be pushed
     */
    push(state) {
        this.array.push(state);
        let index = this.array.length - 1;
        let lastState = this.array[index];
        let parent = Math.floor((index - 1) / 2);
        while ((index > 0) &&
            (this.array[parent].countValidValue() > lastState.countValidValue())) {
            this.array[index] = this.array[parent];
            index = parent;
            parent = Math.floor((index - 1) / 2);
        }
        this.array[index] = lastState;
    }
    /**
     * Pop the state from the heap.
     *
     * @returns {State} the removed state.
     */
    pop() {
        let output = this.array[0];
        if (this.array.length <= 1) {
            return this.array.pop();
        }
        this.array[0] = this.array.pop();
        let currentSize = this.array.length;
        /** @type {number} */
        let smallerChild;
        let index = 0;
        let first = this.array[0];
        while (index < Math.floor(currentSize / 2)) {
            let left = 2 * index + 1;
            let right = left + 1;
            if ((right < currentSize) &&
                (this.array[left].countValidValue() > this.array[right].countValidValue())) {
                smallerChild = right;
            }
            else {
                smallerChild = left;
            }
            if (first.countValidValue() <= this.array[smallerChild].countValidValue()) {
                break;
            }
            this.array[index] = this.array[smallerChild];
            index = smallerChild;
        }
        this.array[index] = first;
        return output;
    }
    /** Checks if heap is empty.
     *
     * @returns {boolean} true if heap is empty
     */
    isEmpty() {
        return (this.array.length === 0);
    }
}

module.exports = StateHeap;