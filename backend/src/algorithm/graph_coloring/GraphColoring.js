/**
 * @callback doneFunction - the function after the algorithm is done
 * @param {boolean} success - if the backtracking is a success
 * @param {Object} assignment - the total assignment
 * 
 * @callback loadingFunction - the function to send loading progress
 * @param {number} progress - the progress; will be between 0 and 1.
 * 
 * @typedef {number} GraphNode - a graph node, must be positive integers
 * @typedef {number} Value - a value, must be positive integers
 */

const StateHeap = require("./StateHeap");
const Constraint = require("./Constraint");
const State = require("./State");

class GraphColoring {
    /**
     * The class of the graph coloring.
     * @param {Constraint} constraint - the constraint of the graph coloring
     * @param {number} domainSize - the amount of the domain
     * @param {doneFunction} done - the callback function after the algorithm is done
     * @param {loadingFunction} loading - the callback function after each progress
     */
    constructor(constraint, domainSize, done, loading) {
        this.constraint = constraint;
        this.domainSize = domainSize;
        
        /** @type {State} */
        this.longestAssignment = null;
        this.longestAssignmentLength = 0;
        this.callCount = 0;
        this.done = done;
        this.loading = loading;
    }

    runBacktrack() {
        this.longestAssignment = State.fromDomainSize(
            this.constraint, this.domainSize);
        let output = this.backtrackRecursive(this.longestAssignment);
        if (output === null) {
            this.done(false, this.longestAssignment);
        } else {
            this.done(true, output);
        }
    }

    /**
     * Perform the backtracking
     * 
     * @param {State} state - the current state of backtracking
     * @returns {?State} the resulting assignment. Null if fails.
     */
    backtrackRecursive(state) {
        let assignedCount = state.countAssigned();
        if (assignedCount == this.constraint.size) {
            return state;
        }
        // Print state
        this.callCount++;
        if (this.longestAssignmentLength < assignedCount) {
            this.loading(assignedCount / this.constraint.size);
            this.longestAssignment = state;
            this.longestAssignmentLength = assignedCount;
        }

        let nextNode = this.chooseNextNode(state);
        let nextStates = this.generateNextStates(state, nextNode);

        while (!nextStates.isEmpty()) {
            let nextState = nextStates.pop();
            let result = this.backtrackRecursive(nextState);
            // console.log(result);
            if (result !== null) {
                return result;
            }
        }
        return null;
    }

    /**
     * Chooses the next node based on the state.
     * 
     * @param {State} state - the current state
     * @returns {GraphNode} the next node
     */
    chooseNextNode(state) {
        let candidate = state.minimumRemainingValue();
        return this.constraint.mostConstrainingNode(candidate);
    }

    /**
     * Generate all the next possible states, store it to a heap.
     * 
     * @param {State} state - the current state
     * @param {GraphNode} currentNode - the current node
     * @returns {StateHeap} the heap
     */
    generateNextStates(state, currentNode) {
        let output = new StateHeap();
        for (let value = 0; value < this.domainSize; value++) {
            let nextState = state.assign(currentNode, value);
            if (nextState === null) {
                continue;
            }
            output.push(nextState);
        }
        return output;
    }
}

module.exports = GraphColoring;
