const State = require('./State');

function standardDeviation(values) {
    var avg = average(values);

    var squareDiffs = values.map(function (value) {
        var diff = value - avg;
        var sqrDiff = diff * diff;
        return sqrDiff;
    });

    var avgSquareDiff = average(squareDiffs);

    var stdDev = Math.sqrt(avgSquareDiff);
    return stdDev;
}

function average(data) {
    var sum = data.reduce(function (sum, value) {
        return sum + value;
    }, 0);

    var avg = sum / data.length;
    return avg;
}

class CustomState extends State {
    constructor(constraint, array, classData, sessionCount) {
        super(constraint, array);
        this.classData = classData;
        this.sessionCount = sessionCount;
    }

    static fromDomainSize(constraint, domainSize, classData, sessionCount) {
        /** @type {Set<number>[]} */
        let newArray = [];
        for (let _ = 0; _ < constraint.size; _++) {
            /** @type {Set<number>} */
            let newSet = new Set();
            for (let value = 0; value < domainSize; value++) {
                newSet.add(value);
            }
            newArray.push(newSet);
        }
        return new CustomState(constraint, newArray, classData, sessionCount);
    }

    /**
     * This method is hacked so that instead of the usual heuristics, this
     * method instead returns the standard deviation of the counter.
     */
    countValidValue() {
        let sessionCapacity = new Array(this.sessionCount).fill(0);
        for (let [classIndex, sessionSet] of this.array.entries()) {
            if (sessionSet.size == 1) {
                let session = sessionSet.values().next().value;
                sessionCapacity[session] += this.classData[classIndex].maxCapacity;
            }
        }
        // console.log(sessionCapacity);
        
        return standardDeviation(sessionCapacity);
    }

    assign(node, value) {
        /** @type {Set<GraphNode>[]} */
        let newArray = [];
        for (let row_set of this.array) {
            newArray.push(new Set(row_set));
        }
        newArray[node] = new Set([value]);
        let newState = new CustomState(
            this.constraint, 
            newArray, 
            this.classData, 
            this.sessionCount);
        let success = newState.forwardCheck(node);
        if (success) {
            return newState;
        }
        else {
            return null;
        }
    }
}

module.exports = CustomState;