const GraphColoring = require('./GraphColoring');
const CustomState = require('./CustomState')

class CustomGraphColoring extends GraphColoring {
    constructor(constraint, domainSize, done, loading, classData) {
        super(constraint, domainSize, done, loading);
        this.classData = classData;
    }

    runBacktrack() {
        this.longestAssignment = CustomState.fromDomainSize(
            this.constraint, this.domainSize, this.classData, this.domainSize);
        let output = this.backtrackRecursive(this.longestAssignment);
        if (output === null) {
            this.done(false, this.longestAssignment);
        } else {
            this.done(true, output);
        }
    }
}

module.exports = CustomGraphColoring;