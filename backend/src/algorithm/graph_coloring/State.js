class State {
    /**
     * The state class.
     *
     * @param {Constraint} constraint - the constraint.
     * @param {Set<GraphNode>[]} array - the array.
     */
    constructor(constraint, array) {
        this.constraint = constraint;
        this.array = array;
    }
    /**
     * Create a new state, assign one node of it with a value, then clean
     * up with forward checking.
     *
     * @param {GraphNode} node - the node to be assigned
     * @param {Value} value - the value too which the node is assigned
     * @returns {?State} a new state; returns null if assignment fails/
     */
    assign(node, value) {
        /** @type {Set<GraphNode>[]} */
        let newArray = [];
        for (let row_set of this.array) {
            newArray.push(new Set(row_set));
        }
        newArray[node] = new Set([value]);
        let newState = new State(this.constraint, newArray);
        let success = newState.forwardCheck(node);
        if (success) {
            return newState;
        }
        else {
            return null;
        }
    }
    /**
     * Counts the total of valid values in the array.
     *
     * @returns {number} the value.
     */
    countValidValue() {
        let validValueCount = 0;
        for (let row of this.array) {
            validValueCount += row.size;
        }
        return validValueCount;
    }
    /**
     * Do a forward checking on a node.
     *
     * @param {number} node - the starting node for forward checking.
     * @returns {boolean} true if there's something reduced from the state.
     */
    forwardCheck(node) {
        let arcs = this.fillInitialArcs(node);
        while (arcs.length > 0) {
            let { first: revisedNode, second: revisingNode } = arcs.pop();
            let isReduced = this.arcReduce(revisedNode, revisingNode);
            if (isReduced) {
                if (this.array[revisedNode].size == 0) {
                    return false;
                }
                for (let newNode of this.constraint.iterateOverNodes(revisedNode, revisingNode)) {
                    arcs.push({ first: newNode, second: revisedNode });
                }
            }
        }
        return true;
    }
    /**
     * Fill initial arcs for forward checking.
     *
     * @param {number} node - the node to be filled in.
     * @returns {{first: GraphNode, second: GraphNode}[]} every arc, in pairs.
     */
    fillInitialArcs(node) {
        /** @type {{first: GraphNode, second: GraphNode}[]} */
        let output = [];
        for (let node2 = 0; node2 < this.constraint.size; node2++) {
            if (this.constraint.includes(node, node2)) {
                output.push({
                    first: node2,
                    second: node
                });
            }
        }
        return output;
    }
    /**
     * Remove the value from revised node if for such value there's no
     * value in revising mode that satisfies the constraint.
     *
     * @param {GraphNode} revisedNode - the revised node to be cleaned up
     * @param {GraphNode} revisingNode - the revising node
     * @returns {boolean} true if any change were made.
     */
    arcReduce(revisedNode, revisingNode) {
        let anyChange = false;
        for (let value of this.iterateOverValues(revisedNode)) {
            if (!this.validValueExists(revisedNode, value, revisingNode)) {
                this.array[revisedNode].delete(value);
                anyChange = true;
            }
        }
        return anyChange;
    }
    /**
     * Check if for such value in revised node, there's a value in
     * revising node that satisfies the constraint.
     *
     * @param {GraphNode} revisedNode - the revised node
     * @param {Value} value - the value to be revised
     * @param {GraphNode} revisingNode - the revising node
     * @returns {boolean} true if there's any
     */
    validValueExists(revisedNode, value, revisingNode) {
        for (let value2 of this.iterateOverValues(revisingNode)) {
            if (this.constraint.validValueSet(revisedNode, value, revisingNode, value2)) {
                return true;
            }
        }
        return false;
    }
    /**
     * Iterates over every available value in a node.
     *
     * @param {GraphNode} node - the node in which the values are iterated.
     * @yields {Value} each value of the node
     */
    *iterateOverValues(node) {
        for (let value of Array.from(this.array[node])) {
            yield value;
        }
    }
    /**
     * Returns the amount of assigned nodes.
     *
     * @returns {number} the amound of assigned nodes.
     */
    countAssigned() {
        let total = 0;
        for (let row of this.array) {
            if (row.size == 1) {
                total++;
            }
        }
        return total;
    }
    /**
     * Returns if the state is failing (i.e. has nodes with no possible values)
     *
     * @returns {boolean} true if the state is failing
     */
    // Wait why even I added this? Ngurang ngurangin coverage aja
    // isFailing() {
    //     for (let row of this.array) {
    //         if (row.size === 0) {
    //             return true;
    //         }
    //     }
    //     return false;
    // }

    /**
     * Returns the set of unassigned nodes that has the least value.
     *
     * @returns {GraphNode[]} the set of node with minimum remaining values.
     */
    minimumRemainingValue() {
        /** @type {?GraphNode} */
        let minVal = null;
        /** @type {GraphNode[]} */
        let candidate = [];
        for (let [index, row] of this.array.entries()) {
            let length = row.size;
            if (length > 1) {
                if ((minVal == null) || (length < minVal)) {
                    minVal = length;
                    candidate = [index];
                }
                else if (length == minVal) {
                    candidate.push(index);
                }
            }
        }
        return candidate;
    }
    /**
     * Create a new (full) State with set initialized depending on domain size.
     *
     * @param {Constraint} constraint - the constraint
     * @param {number} domainSize - domain size, an integer
     * @returns {State} a new State object
     */
    static fromDomainSize(constraint, domainSize) {
        /** @type {Set<number>[]} */
        let newArray = [];
        for (let _ = 0; _ < constraint.size; _++) {
            /** @type {Set<number>} */
            let newSet = new Set();
            for (let value = 0; value < domainSize; value++) {
                newSet.add(value);
            }
            newArray.push(newSet);
        }
        return new State(constraint, newArray);
    }
}

module.exports = State