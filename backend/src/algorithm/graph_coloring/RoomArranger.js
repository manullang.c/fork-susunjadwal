/**
 * 
 * @param {number[]} classCapacity - the class capacity array
 * @param {number[]} rooms - rooms capacity amount
 */
function arrangeRooms(classCapacity, rooms) {
    let classSorted = Array.from(classCapacity.entries()).sort((a, b) => (a[1] - b[1]));
    let assigned = new Array(classCapacity.length);

    let roomSorted = Array.from(rooms.entries()).sort((a, b) => (b[1] - a[1]));
    let s = new Set(roomSorted.map(v => v[1]))

    for (const classEntry of classSorted) {
        let maxSubset = knapsack(classEntry[1], roomSorted).subset;
        // console.log(`${classEntry} -> ` + maxSubset);
        let newRoomSorted = [];
        assigned[classEntry[0]] = [];
        for (const room of roomSorted) {
            let hasElement = false;

            for (let i = 0; i < maxSubset.length; i++) {
                if (room[1] === maxSubset[i]) {
                    maxSubset[i] = null;
                    hasElement = true;
                    break;
                }
            }
            if (!hasElement) {
                newRoomSorted.push(room);
            } else {
                assigned[classEntry[0]].push(room[0]);
            }
        }
        roomSorted = newRoomSorted;
        // console.log(`...(` + assigned[classEntry[0]] + ')');

    }
    return assigned;
}

/**
 * Do the modified knapsack problem.
 * 
 * @param {number} limit - the limit
 * @param {[number, number][]} values - not actually a set; the collection of entries
 */
function knapsack(limit, values) {
    // Populate base cases
    let n = values.length;
    let weight = values;
    /** @type {{ value: number, subset: number[] }[][]} */
    let mat = []
    for (let i = 0; i < n + 1; i++) {
        let row = []
        for (let j = 0; j < limit + 1; j++) {
            row.push({
                value: 0,
                subset: []
            });
        }
        mat.push(row)
    }

    for (let item = 1; item <= n; item++) {
        for (let capacity = 1; capacity <= limit; capacity++) {
            let maxValWithoutCurrent = mat[item - 1][capacity].value;
            let maxArrWithoutCurrent = mat[item - 1][capacity].subset;

            let maxValWithCurrent = 0;
            let maxArrWithCurrent = [];

            let currentWeight = weight[item - 1][1];
            if (capacity >= currentWeight) {
                maxValWithCurrent = values[item - 1][1];

                maxArrWithCurrent = [maxValWithCurrent];
                let remainingCapacity = capacity - currentWeight;
                maxValWithCurrent += mat[item - 1][remainingCapacity].value;
                maxArrWithCurrent = maxArrWithCurrent.concat(mat[item - 1][remainingCapacity].subset);
            }
            if (maxValWithCurrent > maxValWithoutCurrent) {
                mat[item][capacity].value = maxValWithCurrent;
                mat[item][capacity].subset = maxArrWithCurrent;
            } else {
                mat[item][capacity].value = maxValWithoutCurrent;
                mat[item][capacity].subset = maxArrWithoutCurrent;
            }
        }
    }

    return mat[n][limit];
}

/**
 * 
 * @param {*} roomsData - the rooms data parsed from excel
 * @param {Map<number, {id: string, rooms: string[], maxCapacity: number}[]} collector - the collector from trasncriber
 */
function assignRooms(roomsData, collector) {
    let roomsId = [];
    let success = true;
    let roomsCapacity = [];
    
    for (const room of roomsData.slice(1)) {
        roomsId.push(room[1]);
        roomsCapacity.push(Math.ceil(room[5]));
    }

    for (const [session, subjects] of collector) {
        // Collect into an array
        let classCapacity = [];
        for (const subject of subjects) {
            classCapacity.push(subject.maxCapacity);
        }
        
        let r = arrangeRooms(classCapacity, roomsCapacity);

        for (let index = 0; index < r.length; index++) {
            const assignedRooms = r[index];
            if (assignedRooms.length === 0) {
                success = false;
            }
            for (const roomIndex of assignedRooms) {
                subjects[index].rooms.push(roomsId[roomIndex]);
            }
            subjects[index].rooms.sort();
        }
        
    }
    return success;
}

module.exports = assignRooms;