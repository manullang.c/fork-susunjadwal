/**
 * This module transcribes the file to the constraint array
 * 
 * How to use this module:
 * 1.   Prepare the data (rooms and schedule) using the module parsingFile provided
 *      by src/algorithm/Main. Take the arraydata.
 * 2.   Get also the date data.
 * 3.   Construct the new class Transcriber with arguments as described.
 * 4.   Call Transcriber.runAlgorithm(). What it returns are described on the method.
 * 5.   If it does perform successfully, use src/algorithm/create_xlsx_table to convert
 *      the array to xlsx.
 * 
 * @author Aloysius Kurnia Mahendra
 * 
 * ===============================================
 * 
 * @typedef {{ day: number, month: string, year: number }} Day
 * 
 * @callback loadingFunction - the function to send loading progress
 * @param {number} progress - the progress; will be between 0 and 1.
 */

const Constraint = require('./Constraint');
const GraphColoring = require('./CustomGraphColoring');
const arrangeRooms = require('./RoomArranger');


class Transcriber {
    /**
     * 
     * @param {Object[]} scheduleData - Schedule arraydata taken from Nicky's parsingPile
     * @param {Object[]} roomsData - Rooms arraydata taken from Nicky's parsingPile
     * @param {Day[]} dayList - The day list, taken from date_receive
     * @param {*} waitingFunction - Waiting function, in case of Yosua actually implemeting the loading bar
     * @param {*} sessionsPerDate - Sessions per date, also taken from date_receive
     */
    constructor(scheduleData, roomsData, dayList, waitingFunction, sessionsPerDate) {
        this.scheduleData = scheduleData;
        this.roomsData = roomsData;
        /** @type {Map<string, string[]>} */
        this.bucket = new Map();
        /** @type {Map<string, number>} */
        this.toIndex = new Map();
        /** @type {Schedule[]} */
        this.fromIndex = [];

        this.collect();

        this.dayList = dayList;
        this.sessionsPerDate = sessionsPerDate;
        this.waitingFunction = waitingFunction;
    }

    /**
     * Register data to bucket
     * @param {string} id - the id for the schedule
     * @param {string} session - the session (day and session) of the schedule
     * @param {string} program - the study program of the class
     */
    registerToBucket(id, session, program) {
        const key = `${session} ${program}`;
        if (this.bucket.has(key)) {
            this.bucket.get(key).push(id);
        } else {
            this.bucket.set(key, [id]);
        }
    }

    /**
     * Collects data.
     */
    collect() {
        let indexNo = 0;
        for (const entry of this.scheduleData.slice(1)) {
            const id = entry[0];

            if (id === "") {
                continue;
            }
            const name = entry[1];
            const session = entry[8];
            const program = entry[4];
            const difficulty = entry[9];
            const classCount = entry[10];
            const classCapacity = entry[11];


            if (program !== 'ALL') {
                if (program === 'FEB') {
                    this.registerToBucket(id, session, 'IE');
                    this.registerToBucket(id, session, 'EMA');
                    this.registerToBucket(id, session, 'IEI-BI');
                    this.registerToBucket(id, session, 'EAK');
                } else {
                    this.registerToBucket(id, session, program);
                }
                this.fromIndex.push({
                    id: id,
                    name: name,
                    session: session,
                    program: program,
                    maxCapacity: this.totalMaxCapacity(classCount, classCapacity),
                    rooms: []
                });
                this.toIndex.set(id, indexNo++);
            }
        }
    }

    totalMaxCapacity(classCount, classCapacity) {
        let maxCapPerClass = [0, 21, 45, 60][classCapacity];
        return classCount * maxCapPerClass;
    }

    /**
     * Runs the graph coloring algorithm.
     * @returns {success: boolean, reason: ?string, output: Object[]}
     * success - is whether the algorithm is successful or not.
     * reason - the reason why does it fail, if it does fail.
     * output - the excel-ready array output, to be made in xlsx.
     */
    runAlgorithm() {
        let constraint = new Constraint(this.createConstraint());
        let result = null;
        let success = true;

        let gc = new GraphColoring(
            constraint,
            this.dayList.length * this.sessionsPerDate,
            (s, assignment) => {
                result = assignment;
                success = s;


            },
            this.waitingFunction,
            this.fromIndex);
        gc.runBacktrack();
        if (!success) {
            return {
                success: false,
                reason: 'sistem tidak dapat menemukan susunan jadwal',
                output: null
            }
        }
        let postprocessResult = this.postprocessState(result);
        if (!postprocessResult.success) {
            return {
                success: false,
                reason: 'sistem dapat menemukan susunan jadwal, tetapi ' +
                    'tidak dapat menemukan susunan ruangan',
                output: null
            }
        }

        return {
            success: true,
            reason: null,
            output: postprocessResult.output
        }
    }

    /**
     * @returns {boolean[][]} The array
     */
    createConstraint() {
        let size = this.fromIndex.length;
        let output = [];
        for (let i = 0; i < size; i++) {
            output.push(new Array(size).fill(false));
        }

        for (let [session, pairs] of this.bucket) {
            // Iterate every combination in pairs
            for (let i = 0; i < pairs.length; i++) {
                for (let j = i + 1; j < pairs.length; j++) {
                    let sched1 = pairs[i];
                    let sched2 = pairs[j];
                    output[this.toIndex.get(sched1)][this.toIndex.get(sched2)] = true;
                    output[this.toIndex.get(sched2)][this.toIndex.get(sched1)] = true;
                }
            }
        }

        return output;
    }

    /**
     * Postprocess the state so that it's readily available as array.
     * 
     * @param {State} state the state
     */
    postprocessState(state) {
        /** @type {Map<number, Schedule[]>} */
        let collector = new Map();
        for (const [index, value] of state.array.entries()) {
            let subjClass = this.fromIndex[index];
            let session = value.values().next().value;

            if (collector.has(session)) {
                collector.get(session).push(subjClass);
            } else {
                collector.set(session, [subjClass]);
            }
        }

        let success = arrangeRooms(this.roomsData, collector);

        // Assign each session number with an actual session.
        let output = [];
        let sessions = this.generateSessions();
        for (const sessionIndex of Array.from(collector.keys()).sort((a, b) => { a - b })) {
            let session = sessions[sessionIndex];
            for (const sessionClass of collector.get(sessionIndex)) {
                for (const room of sessionClass.rooms) {
                    output.push([
                        session[0], session[1], [room, 0, sessionClass.name]
                    ]);
                }
            }
        }
        return { success: success, output: output };
    }

    /**
     * @returns {[string, number][]}
     */
    generateSessions() {
        let output = [];
        for (const day of this.dayList) {
            for (let i = 1; i <= this.sessionsPerDate; i++) {
                output.push([`${day.weekday}, ${day.day} ${day.month}`, i]);
            }
        }
        return output;
    }
}

// const { parse } = require('../Main');
// const { createXlsx } = require('../create_xlsx_table');
// const fs = require('fs');

// function printProgress(progress) {
//     console.log((progress * 100).toFixed(2).padStart(6) + '%');
// }

// function main() {
//     let scheduleData = parse(fs.readFileSync('./files/schedule.xlsx')).arraydata;
//     let roomsData = parse(fs.readFileSync('./files/rooms.xlsx')).arraydata;
//     let transcriber = new Transcriber(
//         scheduleData,
//         roomsData,
//         [
//             { day: 28, month: 'Mar', year: 2012 },
//             { day: 29, month: 'Mar', year: 2012 },
//             { day: 30, month: 'Mar', year: 2012 },
//             { day: 31, month: 'Mar', year: 2012 },
//             { day: 1, month: 'Apr', year: 2012 },
//             { day: 2, month: 'Apr', year: 2012 },
//             { day: 3, month: 'Apr', year: 2012 },
//             { day: 4, month: 'Apr', year: 2012 },
//             { day: 5, month: 'Apr', year: 2012 },
//             { day: 6, month: 'Apr', year: 2012 },
//             { day: 7, month: 'Apr', year: 2012 }
//         ],
//         printProgress,
//         2
//     );
//     let output = transcriber.runAlgorithm();
//     createXlsx(output.output)

// }

// if (require.main == module) {
//     main();
// }

module.exports = Transcriber;