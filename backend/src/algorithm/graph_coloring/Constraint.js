class Constraint {
    /**
     * The constraint class.
     *
     * @param {boolean[][]} assignmentArray the graph.
     */
    constructor(assignmentArray) {
        this.array = assignmentArray;
        this.size = assignmentArray.length;
        /**
         * @type {number[]}
         */
        this.constraintCount = [];
        for (let row of assignmentArray) {
            let counter = 0;
            for (let otherNode of row) {
                if (otherNode) {
                    counter++;
                }
            }
            this.constraintCount.push(counter);
        }
    }
    /**
     * Check if {node1, node2} is in constraint.
     *
     * @param {GraphNode} node1 - the first node
     * @param {GraphNode} node2 - the second node
     * @returns {boolean} true if both node is in constraint.
     */
    includes(node1, node2) {
        return this.array[node1][node2];
    }
    /**
     * Iterates over every node that has a pair with the parameter node.
     * Exclusion denotes which node to omit from the iteration.
     *
     * @param {GraphNode} node - the node to find all matching
     * @param {GraphNode} exclusion - the excluded node
     * @yields {GraphNode} iterated node
     */
    *iterateOverNodes(node, exclusion) {
        let row = this.array[node];
        for (let iteratedNode = 0; iteratedNode < this.size; iteratedNode++) {
            if ((row[iteratedNode]) && (iteratedNode != exclusion)) {
                yield iteratedNode;
            }
        }
    }
    /**
     * Check if the combinations of values per node is valid.
     * (i.e. if they're in constraint, their values aren't the same.)
     *
     * @param {GraphNode} node1 - first node
     * @param {Value} value1 - value of node1
     * @param {GraphNode} node2 - second node
     * @param {Value} value2 - value of node2
     * @returns {boolean} true if it's valid set.
     */
    validValueSet(node1, value1, node2, value2) {
        if (this.includes(node1, node2)) {
            if (value1 === value2) {
                return false;
            }
        }
        return true;
    }
    /**
     * Get the most constraining node from the candidates
     *
     * @param {GraphNode[]} candidate - the candidates
     * @returns {GraphNode} - the most constraining node
     */
    mostConstrainingNode(candidate) {
        /**
         * @type {?number}
         */
        let lowestCount = null;
        let lowestNode = candidate[0];
        for (let index of candidate) {
            let count = this.constraintCount[index];
            if ((lowestCount === null) || (count > lowestCount)) {
                lowestCount = count;
                lowestNode = index;
            }
        }
        return lowestNode;
    }
}

module.exports = Constraint;