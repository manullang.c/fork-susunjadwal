const xlsx = require('xlsx');

function getDataFromExcel(buffer) {
    var wb = xlsx.read(buffer,{type:"buffer"});
    var sheetName = wb.SheetNames;
    var data = wb.Sheets[sheetName[0]];
    return data;
}

function excel2JSON(buffer) {
    var data = getDataFromExcel(buffer);
    var jsonData = xlsx.utils.sheet_to_json(data);
    return jsonData;
}

function excel2Array(buffer) {
    var data = getDataFromExcel(buffer);
    var arrayData = xlsx.utils.sheet_to_json(data,{header:1,defval:""});
    return arrayData;
}

/**
 * parsingFile parses buffer into JSON object
 * 
 * @param buffer buffer data of excel file
 * @returns JSON object and array data
 */

function parsingFile(buffer){
    var jsondata = excel2JSON(buffer);
    var arraydata = excel2Array(buffer);
    return {jsondata, arraydata};
}

module.exports.parse = parsingFile;

