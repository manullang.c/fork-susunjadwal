//Requires Firebase to be installed using the command 'npm install firebase'   
var firebase = require('firebase/app')
var admin = require("firebase-admin");

var serviceAccount = require("./serviceAccountKey.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  storageBucket: "trial-project-88bcb.appspot.com",
});

var storageBucket = admin.storage().bucket();

module.exports.storageBucket = storageBucket;
module.exports.firebase = firebase;