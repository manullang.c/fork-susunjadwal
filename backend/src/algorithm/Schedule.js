/*
Schedule.js is constructor class and data structure for storing jadwal ujian purpose.

Variables:
 - maxSessionsPerDay, How many object will limit exam sessions for each day reserved
 - days, How many object will reserved day for exams
 - valid, is data preparation is valid (for exception handling purposes)
 - courses, data of courses faculty reserved
 - rooms, data of rooms available
 - table, data structure created for scheduling algorithm

Methods:
- constructor, @params: sessions, days
- insertCourses, @params: data of classes faculty reserved (in array)
- insertRooms, @params: data of rooms, in array
- createTable, @params: -
- dataPreparation, @params: coursesData, roomsData. For easy use and combine of all data preps methods.

How to use:
 - Import this module first
 - Construct the object with this class, example: jadwal = new Schedule();
 - call dataPreparations, it will call and insert all data you needs and create a table which is ready to use.
   output is true if init is success, false if failed
   its given output boolean so you can do this

   if (example.dataPreparations(...)){
     //do scheduling algorithms
   }

This class isn't finish yet! I haven't adding scheduling algorithm. But what I wrote here is stable and ready to use.
To author do (ordered by priority) :
- Create scheduling algorithm, really need help!

Please don't hesitate to ask me more
@author = Muhamad Nicky Salim
*/

class Schedule {
  constructor(sessions, days) {
    this.maxSessionsPerDay = sessions;
    this.days = days;
    if (sessions > 0 && days > 0) {
      this.valid = true;
    } else {
      this.valid = false;
    }
  }

  //methods
  insertCourses(data) {
    if (this.valid) {
      this.courses = data;
    }
  }

  insertRooms(data) {
    if (this.valid) {
      var result = [];
      var added;
      var inspected;

      for (var i = 0; i < data.length; i++) {
        inspected = data[i];

        added = [inspected[1], inspected[2], inspected[5]]; //[room name, class type, exam capacity]
        result.push(added);
      }

      this.rooms = result;
    }
  }

  createTable() {
    if (this.valid) {
      var result = [];
      var times;
      var details;
      var selected;
      var roomName;
      var capacity;

      for (var day = 1; day <= this.days; day++) {
        for (var session = 1; session <= this.maxSessionsPerDay; session++) {
          for (var roomNumber = 0; roomNumber < this.rooms.length; roomNumber++) {
            times = [day, session];

            selected = this.rooms[roomNumber];
            roomName = selected[0];
            capacity = selected[2];

            details = [roomName, capacity, null];
            times.push(details);// [days, sessions, [room, capacity, course]]
            result.push(times);
          }
        }
      }
    }

    this.table = result;
  }

  dataPreparations(coursesData, roomsData) { //just for readability purposes
    this.insertCourses(coursesData);
    this.insertRooms(roomsData);
    this.createTable();

    return this.valid;
  }
}

module.exports = Schedule;