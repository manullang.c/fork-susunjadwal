const {firebase, storageBucket} = require("./FirebaseService")

const xlsx = require('xlsx');

function createXlsx(data_array) {
    let wb = xlsx.utils.book_new()
    let wsName = "Jadwal ujian";

    let data = {};

    register(data, data_array);
    let maxLengths = getMaxLengths(data);

    /* make worksheet */
    let wsData = [
        ['Jadwal kuliah'],
        [],
    ];
    let mergeData = [];
    let [headerXl, dayOutput] = fillHeader(data, mergeData);
    wsData.push(headerXl);
    
    let arrayOutput = [];
    wsData.push(...fillTableData(data, maxLengths, mergeData, arrayOutput));
    
    let ws = xlsx.utils.aoa_to_sheet(wsData);
    
    ws['!merges'] = mergeData;
    xlsx.utils.book_append_sheet(wb, ws, wsName);
    xlsx.writeFile(wb, 'files/output_jadwal_ujian.xlsx');
    // TODO: Replace this with actual name of file
    const nameOfFile = getCurrentDate(); 
    xlsx.writeFile(wb, 'files/history/' + nameOfFile + ".xlsx")
    return [dayOutput, arrayOutput]
}

function getCurrentDate(){
    var currentDate = new Date();

    var date = currentDate.getDate();
    var month = currentDate.getMonth(); //Be careful! January is 0 not 1
    var year = currentDate.getFullYear();
    var seconds = currentDate.getSeconds();
    var minutes = currentDate.getMinutes();
    var hour = currentDate.getHours();

    return date + "_" +(month + 1) + "_" + year + "_" + hour + "_" + minutes + "_" + seconds;
}


function register(storage, data_array) {
    let available_sessions = [];
    data_array.forEach(data => {
        let [date, session, [room, , name]] = data;
        if (!available_sessions.includes(session)) {
            available_sessions.push(session);
        }
        if (storage[date] === undefined) {
            storage[date] = {}
        }
        if (storage[date][session] === undefined) {
            storage[date][session] = []
        }
        storage[date][session].push([room, name]);
    });

    fillMissingSessions(storage, available_sessions);
}

function fillMissingSessions(storage, session_list) {
    Object.keys(storage).forEach(day => {
        session_list.forEach(session => {
            if (storage[day][session] === undefined) {
                storage[day][session] = [];
            }
        })
    })
}

function getMaxLengths(storage) {
    let sessionLength = {};

    Object.keys(storage).forEach(date => {
        Object.keys(storage[date]).forEach(session => {
            if (sessionLength[session] === undefined) {
                sessionLength[session] = 0;
            }
            if (sessionLength[session] < storage[date][session].length) {
                sessionLength[session] = storage[date][session].length;
            }
        })
    })

    return sessionLength;
}

function fillHeader(schedules, mergeData) {
    let output = ["Sesi"];
    let output2 = [];
    let yPos = 1;
    Object.keys(schedules).forEach(day => {
        output.push(day, null);
        output2.push(day);
        mergeData.push({
            s: { c: yPos++, r: 2 },
            e: { c: yPos++, r: 2 }
        });
    });

    // Merge title
    mergeData.push({
        s: { c: 0, r: 0 },
        e: { c: yPos - 1, r: 0 }
    });
    return [output, output2];
}

function fillTableData(data, maxPerSession, mergeData, arrayOutput) {
    // Fill each columns
    let output = []
    let yPos = 3
    Object.keys(maxPerSession).forEach(session => {
        let fill = fillSession(data, maxPerSession[session], session, arrayOutput);
        output.push(...fill);
        mergeData.push({
            s: { c: 0, r: yPos },
            e: { c: 0, r: yPos + maxPerSession[session] - 1 }
        });
        yPos += maxPerSession[session];

    });
    return output;
}

function fillSession(data, maxInSession, session, arrayOutput) {
    let output = [];
    let arrayOutputSession = [...Array(Object.keys(data).length)].map(e => Array(maxInSession));
    for (let i = 0; i < maxInSession; i++) {
        let row = [session];

        let j = 0;
        Object.keys(data).forEach(day => {

            let schedule, room, schedx, roomx;
            if (data[day][session][i] === undefined) {
                [room, schedule] = [null, null];
                [roomx, schedx] = ['_', '_']
            } else {
                [roomx, schedx] = [room, schedule] = data[day][session][i];
            }
            row.push(schedule, room);
            arrayOutputSession[j][i] = [roomx, schedx]
            j++;
        })
        output.push(row);
    }
    arrayOutputSession.unshift(session)
    arrayOutput.push(arrayOutputSession);
    return output;
}

module.exports.createXlsx = createXlsx;

// createXlsx([
//     ['Senin, 23 April', 1, ['A1106', 30, 'Akuntansi 1']],
//     ['Senin, 23 April', 1, ['A1107', 30, 'Akuntansi 2']],
//     ['Senin, 23 April', 2, ['A1108', 30, 'Perpajakan 1']],
//     ['Senin, 23 April', 2, ['A1109', 30, 'Perpajakan 2']],
//     ['Selasa, 24 April', 1, ['A1111', 30, 'Ekonomi makro']],
//     ['Selasa, 24 April', 2, ['A1112', 30, 'MPKT A']],
//     ['Selasa, 24 April', 2, ['A1113', 30, 'MPKT B']],
//     ['Selasa, 24 April', 3, ['A1114', 30, 'Bahasa Inggris']],
//     ['Rabu, 25 April', 3, ['A1114', 30, 'Bahasa Inggris']],
// ])