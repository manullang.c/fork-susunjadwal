const scheduleHeaderArray = ["kode mk",
                       "nama mata kuliah",
                       "sks",
                       "jenis mata kuliah",
                       "asal prodi peserta matkul",
                       "jadwal perkuliahan by semester 1=gasal; 2= genap; 3=gasal&genap",
                       "posisi semester ke (by sequence)",
                       "jenis ujian (1=closed book; 2=openbook; 3=paper)",
                       "jadwal kuliah",
                       "tingkat kesulitan (1=ringan; 2=sedang; 3=sulit)",
                       "jumlah kelas (paralel)",
                       "ukuran kelas paralel (1= kurang dari 22mhs; 2=22-45; 3=lebih dari45)"];

const roomHeaderArray = ["no.", 
                   "ruang", 
                   "jenis kelas", 
                   "kap. utk kuliah", 
                   "cap. factor utk ujian", 
                   "kap. ruang ujian"];

function validateRuang(roomArray) {
    if (roomArray[0].length != roomHeaderArray.length) {
        return false;
    }
    var flags = [];
    for (let i = 0; i < roomArray[0].length; i++){
        flags.push(roomArray[0][i].toLowerCase().trim() == roomHeaderArray[i])
    }
    return flags.every((flag)=>flag);
}

function validateJadwal(scheduleArray) {
    if (scheduleArray[0].length != scheduleHeaderArray.length) {
        return false
    }
    var flags = [];
    for (let i = 0; i < scheduleArray[0].length; i++){
        flags.push(scheduleArray[0][i].toLowerCase().trim() == scheduleHeaderArray[i])
    }
    return flags.every((flag)=>flag);
}

module.exports.validateRoom = validateRuang;
module.exports.validateSchedule = validateJadwal;