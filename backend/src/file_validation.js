const xlsx = require('xlsx');

function numToSSColumn(num){
    if (num == 0) return undefined
    var SSCol = '', letterNum;
    while (num > 0) {
        letterNum = (num - 1) % 26;
        SSCol = String.fromCharCode(65 + letterNum) + SSCol;
        num = (num - letterNum)/26 | 0;
    }
    return SSCol;
  }

function convertToCell(i, j) {
    let row = (i + 1).toString();
    let col = numToSSColumn(j + 1);
    return { col, row };
}
/**
 * 
 * checkForHole will check for empty cell in an Excel file
 * 
 * @param {ExcelFile} data excel file of data
 * 
 * @returns nullData if the data has empty cells
 * @returns "the data is complete" if the data has no empty cells
 * 
 */
function checkForHole(arrayData){
    var hasNull = false;
    var nullData = [];
    for (let i = 0; i < arrayData.length; i++) {
        for (let j = 0; j < arrayData[i].length; j++) {
            if (arrayData[i][j] == "") {
                let { col, row } = convertToCell(i, j);
                hasNull = true;
                nullData.push(col + row);
            }
        }
    }
    return {hasNull, nullData};
}

module.exports.checkForHole = checkForHole;
module.exports.numToSSColumn = numToSSColumn;

