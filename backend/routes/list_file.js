const express = require('express');
const fs = require('fs');
const folderName = 'files/history/'

router = express.Router();

router.get('/', function (req, res, next) {
    //returns file metadata
    fs.readdir(folderName, function (err, files){
        listOfFiles = [];
        if (err){
            console.log('Unable to scan directory: ' + err);
            return 
        } files.forEach(function (file) {
            let filesize = fs.statSync(folderName + file)["size"];
            let pushed = {"name": file, "size": filesize}
            listOfFiles.push(pushed);
        });
        console.log(listOfFiles);
        res.status(200).send({"items": listOfFiles});
    });
});

router.get('/download/:filename', function (req, res, next) {
    filename = folderName + req.params.filename;
    console.log(filename);
    console.log(fs.existsSync(filename));
    if (fs.existsSync(filename)) {
        res.status(200).download(filename);
    } else {
        res.status(500).send({valid: false});
    }
});

router.get('/delete/:filename', function(req, res, next) {
    filename = folderName + req.params.filename;
    console.log(filename);
    console.log(fs.existsSync(filename));
    if (fs.existsSync(filename)){
        fs.unlinkSync(filename);
        res.status(200).send({valid: true});
    } else {
        res.status(500).send({valid: false});
    }
});

module.exports = router;