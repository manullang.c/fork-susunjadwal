const express = require('express');
var router = express.Router();
const formatValidator = require("../src/format_validator.js");
const fileVerification = require("../src/file_validation.js");
const mainFunction = require("../src/algorithm/Main.js")
const Transcriber = require('../src/algorithm/graph_coloring/Transcriber');
const fs = require('fs');
const { createXlsx } = require('../src/algorithm/create_xlsx_table');

function preprocessDate(dateList) {
    let output = [];
    let dateIntList = dateList.map(Date.parse);
    dateIntList.sort((a, b) => a - b)
    for (const dateInt of dateIntList) {
        
        let date = new Date(dateInt + 7 * 60 * 60 * 1000);
        let day = ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu'][
            date.getDay()
        ]
        let month = [
            'Januari', 'Februari', 'Maret',
            'April', 'Mei', 'Juni',
            'Juli', 'Agustus', 'September',
            'Oktober', 'November', 'Desember'][
            date.getMonth()
        ]
        output.push({
            weekday: day,
            day: date.getDate(),
            month: month,
            year: date.getFullYear()
        });
    }

    return output;
}

/* GET JSON data. */
router.post('/', function (req, res, next) {

    var scheduleFile = fs.readFileSync('./files/schedule.xlsx');
    var roomFile = fs.readFileSync('./files/room.xlsx');
    var scheduleData = mainFunction.parse(scheduleFile);
    var roomData = mainFunction.parse(roomFile);
    if (!formatValidator.validateRoom(roomData.arraydata) || !formatValidator.validateSchedule(scheduleData.arraydata)) {
        res.status(200).send({ 
            success: false,
            reason: "file yang dimasukkan tidak valid" });
    } else {
        scheduleJSON = scheduleData.jsondata
        roomJSON = roomData.jsondata
        scheduleMessage = fileVerification.checkForHole(scheduleData.arraydata)
        roomMessage = fileVerification.checkForHole(roomData.arraydata)

        console.log(preprocessDate(req.body.dates));
        
        let transcriber = new Transcriber(
            scheduleData.arraydata,
            roomData.arraydata,
            preprocessDate(req.body.dates),
            (s) => {
                console.log(s);
            },
            req.body.sessionPerDay
        );
        
        let output = transcriber.runAlgorithm();
        console.log(output.reason);
        if (!output.success) {
            res.status(200).send({
                success: false,
                reason: output.reason
            });
        }
        let table = createXlsx(output.output);
        console.log(table[0]);
        
        return res.status(200).send({
            success: true,
            result: table,
        });
    }
});

module.exports = router;
