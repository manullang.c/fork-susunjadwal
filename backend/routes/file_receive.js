const express = require('express');
const multer = require('multer');
const cors = require('cors');
const main = require('../src/algorithm/Main.js');

var router = express.Router();
// set up cors
var whitelist = [
  'http://localhost:3001', 
  'https://susun-jadwal-staging.herokuapp.com',
]
var corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1 || !origin) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  }
}


function takeLastElement(str) {
  var splitted = str.split('.');
  var lastElement = splitted[splitted.length - 1];
  return lastElement;
}

function storageFunc(dest) {
  return multer.diskStorage({
    destination: (_req, _file, cb) => {
      cb(null, './files');
    },
    filename: (_req, file, cb) => {
      var extension = takeLastElement(file.originalname);
      cb(null, dest + '.' + extension);
    }
  });
}
function upload(dest) {
  return multer({
    storage : storageFunc(dest),
    fileFilter: (_req, file, cb) => {
      xlsxFlag = file.mimetype == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' && takeLastElement(file.originalname) == 'xlsx';
      xlsFlag = file.mimetype == 'application/vnd.ms-excel' && takeLastElement(file.originalname) == 'xls';
      const isIncorrect = !xlsFlag && !xlsxFlag;
      if (isIncorrect) {
        return cb(new Error('File format must be in xls or xlsx'));
      } else {
        return cb(null, true);
      }
    } 
  }).single('file');
}

// var scheduleStorage = multer.diskStorage({
//   destination: (_req, _file, cb)=>{
//     cb(null, './files');
//   },
//   filename: (_req, file, cb)=>{
//     var extension = takeLastElement(file.originalname);
//     cb(null, 'schedule.' + extension)
//   }
// })

// var roomStorage = multer.diskStorage({
//   destination: (_req, _file, cb)=>{
//     cb(null, './files');
//   },
//   filename: (_req, file, cb)=>{
//     var extension = takeLastElement(file.originalname);
//     cb(null, 'room.' + extension)
//   }
// })

// var uploadSchedule = multer({
//   storage: scheduleStorage,
//   fileFilter: (_req, file, cb) => {
//     xlsxFlag = file.mimetype == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' && takeLastElement(file.originalname) == 'xlsx';
//     xlsFlag = file.mimetype == 'application/vnd.ms-excel' && takeLastElement(file.originalname) == 'xls';
//     if (!xlsFlag && !xlsxFlag) {
//       return cb(new Error('File format must be in xls or xlsx'));
//     } else {
//       return cb(null, true);
//     }
//   }
// }).single('file')

// var uploadRoom = multer({
//   storage: roomStorage,
//   fileFilter: (_req, file, cb) => {
//     xlsxFlag = file.mimetype == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' && takeLastElement(file.originalname) == 'xlsx';
//     xlsFlag = file.mimetype == 'application/vnd.ms-excel' && takeLastElement(file.originalname) == 'xls';
//     if (!xlsFlag && !xlsxFlag) {
//       return cb(new Error('File format must be in xls or xlsx'));
//     } else {
//       return cb(null, true);
//     }
//   }
// }).single('file')

/**
 * postToSchedule will receive schedule file from frontend
 * it will link to "/upload/schedule" and send the file to backend
 * 
 * @param req Request from client
 * @param res Response to client
 * 
 * @returns status 200 and sends JSON object of {message, data, scheduleJSON, scheduleArray} to frontend
 * 
 * 
 */
var postToSchedule = router.post('/schedule', uploadSchedule=upload("schedule"), function (req, res) {
  uploadSchedule(req, res, function () {
    return res.status(200).send({
      message:'Schedule file uploaded',
      data: req.file,
    });
  })
});

/**
 * postToRoom will receive schedule file from frontend
 * it will link to "/upload/room" and send the file to backend
 * 
 * @param req Request from client
 * @param res Response to client
 * 
 * @returns status 200 and sends JSON object of {message, data, roomJSON, roomArray} to frontend
 * 
 * 
 */
var postToRoom = router.post('/room', uploadRoom=upload("room"), function (req, res) {
  uploadRoom(req, res, function () {
    return res.status(200).send({
      message:'Room file uploaded',
      data: req.file
    });
  })
});

module.exports = router;

