var express = require('express');
var multer = require('multer');
var cors = require('cors');

var router = express.Router();
router.post('/', function (req, res) {
    
    const dates = req.body.dates;
    const sessionsPerDate = req.body.sessionsPerDate;
    return res.status(200).json({
        dates: dates,
        sessionsPerDate: sessionsPerDate
    });
});

module.exports = router;