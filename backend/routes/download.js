const express = require('express');
const fs = require('fs');
const filename = 'files/output_jadwal_ujian.xlsx';

router = express.Router();

router.get('/', function (req, res, next) {
    console.log(filename);
    console.log(fs.existsSync(filename));

    if (fs.existsSync(filename)) {
        res.status(200).download(filename);
    } else {
        res.status(500).send({valid: false});
    }
});

module.exports = router;