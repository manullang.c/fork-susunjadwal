# Berkelompok-FEB UI Software Penjadwalan Ujian

PPL Genap 2019/2020 Kelompok A3 dengan proyek FEB UI Software Penjadwalan Ujian

## Overview

Sebuah aplikasi Nodejs + Reactjs (*for now*) untuk menyusun jadwal ujian berdasarkan input 2 file excel, yakni daftar jadwal kuliah harian dan daftar ruangan yang tersedia.

Sementara ini staging dideploy ke Heroku, rencananya akan dikonversi menjadi standalone app dengan Electronjs.

## Prerequisites

- Nodejs (beserta npm)

## Deploy Environment

<http://susun-jadwal-staging.herokuapp.com>

## Quick Guide

- Running the app locally:
  `npm start`
  Runs on <http://localhost:3000> .
  `npm test`
  Runs the test runner.
- Building the app for production:
  `npm run build`
- Note untuk developer: Push perubahan final ke branch **staging**. Setelah disetujui oleh PO, barulah boleh mengirim **merge request** untuk push ke branch **master**.

### Developers

- Aloysius Kurnia Mahendra
- Dimas Krissanto Rahmadi
- Michael Christopher Manullang
- Muhamad Nicky Salim
- Yosua Krisnando Bagaskara
