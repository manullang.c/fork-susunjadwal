/*
NOTE: THIS FILE IS TEMPORARY AND WAS MADE TO MAKE SURE EVERYTHING
WORKS. Please replace/merge with backend development.
*/

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors');

var homePage = require('./routes/users');
var app = express();

app.all('/*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next();
});

app.use(cors());
app.options('*', cors());


var fileReceive = require('./routes/file_receive');
var fileSend = require('./routes/get_table');
var dateReceive = require('./routes/date_receive');
var downloadFile = require('./routes/download');
var listFile = require('./routes/list_file');

var createTable = require('./src/algorithm/create_xlsx_table');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// set up cors
var whitelist = [
  'http://localhost:3001',
  'https://susun-jadwal-staging.herokuapp.com',
  'http://susun-jadwal-staging.herokuapp.com'
]
var corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1 || !origin) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  }
}


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(fileReceive);


app.get('/', (req, res) => res.json({
  text: "Hello World"
}));


app.use('/upload', fileReceive);
app.use('/get_table', fileSend);
app.use('/send_date', dateReceive);
app.use('/download', downloadFile);
app.use('/list_file', listFile);


app.get('/table_test', (req, res) => {
  createTable.createXlsx([
    ['Senin, 23 April', 1, ['A1106', 30, 'Akuntansi 1']],
    ['Senin, 23 April', 1, ['A1107', 30, 'Akuntansi 2']],
    ['Senin, 23 April', 2, ['A1108', 30, 'Perpajakan 1']],
    ['Senin, 23 April', 2, ['A1109', 30, 'Perpajakan 2']],
    ['Selasa, 24 April', 1, ['A1111', 30, 'Ekonomi makro']],
    ['Selasa, 24 April', 2, ['A1112', 30, 'MPKT A']],
    ['Selasa, 24 April', 2, ['A1113', 30, 'MPKT B']],
    ['Selasa, 24 April', 3, ['A1114', 30, 'Bahasa Inggris']]
  ])
  res.render('done');
});


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});


// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


module.exports = app;
