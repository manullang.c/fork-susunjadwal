import React from 'react'
import { unmountComponentAtNode } from "react-dom";
import { shallow, configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ReturnToSplashComponent from './components/common/ReturnToSplashComponent.jsx'
import  Button  from 'react-bootstrap/Button';


configure({ adapter: new Adapter() });
let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

describe('ReturnToSplashComponent', () => {
    it('ReturnToSplashButton should execute onclick function when clicked', () => {
        const func = jest.fn().mockName('returnToSplash');
        const tree = mount(
            <ReturnToSplashComponent returnToSplash = {func}/>
        )
        tree.find(Button).simulate('click');
        expect(func).toHaveBeenCalled();
    });
});