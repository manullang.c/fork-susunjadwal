import React from 'react';
import { unmountComponentAtNode } from "react-dom";
import { shallow, configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import SuccessPageComponent from './components/success_page/SuccessPageComponent.jsx';
import DayDateHeaderComponent from './components/success_page/DayDateHeaderComponent.jsx';
import MataKuliahComponent from './components/success_page/MataKuliahComponent.jsx';

configure({ adapter: new Adapter() });
let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

//18, 24, 25, 26, 56

//56 setNumberOfScrolls(numberOfScrolls)
//64 FaChevronLeft
//104 FaChevronRight

describe('SuccessPageComponent Functions', () => {
    it('When we specify an array that has been formatted a specific way, it displays it according to how it is designed', () => {
        const tree = mount(
            <SuccessPageComponent />
        )
        const testDayDateArray = ["Rabu, 10 Juni", "Kamis, 11 Juni", "Jumat, 12 Juni", "Sabtu, 13 Juni", "Minggu, 14 Juni", "Senin, 15 Juni", "Selasa, 15 Juni", "Rabu, 16 Juni"];
        const testMatkulRuangArray = [[0,[["matkul", "ruang"],["matkul", "ruang"],["matkul", "ruang"]], [["matkul", "ruang"],["matkul", "ruang"],["matkul", "ruang"]], [["matkul", "ruang"],["matkul", "ruang"],["matkul", "ruang"]], [["matkul", "ruang"],["matkul", "ruang"],["matkul", "ruang"]], 
        0, [["matkul", "ruang"],["matkul", "ruang"],["matkul", "ruang"]], [["matkul", "ruang"],["matkul", "ruang"],["_", "_"]], [["matkul", "ruang"],["matkul", "ruang"],["matkul", "ruang"]], [["matkul", "ruang"],["matkul", "ruang"],["matkul", "ruang"]]]]
        const instance = tree.instance();
        instance.setDayDateStringArray(testDayDateArray);
        tree.update();
        instance.setComponents(testMatkulRuangArray);
        tree.update();
        expect(tree.find('.sesi-numbers').length).toBe(1);
        expect(tree.find(DayDateHeaderComponent).length).toBe(4);
        expect(tree.find('.number').length).toBe(1);
        expect(tree.find(MataKuliahComponent).length).toBe(12);
    });
    it('Runs the correct FaChevron Funcs when clicked', () => {
      const tree = mount(
        <SuccessPageComponent />
    )
    const testMatkulRuangArray = [[0,[["matkul", "ruang"],["matkul", "ruang"],["matkul", "ruang"]], [["matkul", "ruang"],["matkul", "ruang"],["matkul", "ruang"]], [["matkul", "ruang"],["matkul", "ruang"],["matkul", "ruang"]], [["matkul", "ruang"],["matkul", "ruang"],["matkul", "ruang"]], 
        0, [["matkul", "ruang"],["matkul", "ruang"],["matkul", "ruang"]], [["matkul", "ruang"],["matkul", "ruang"],["_", "_"]], [["matkul", "ruang"],["matkul", "ruang"],["matkul", "ruang"]], [["matkul", "ruang"],["matkul", "ruang"],["matkul", "ruang"]]]]
    const testDayDateArray = ["Rabu, 10 Juni", "Kamis, 11 Juni", "Jumat, 12 Juni", "Sabtu, 13 Juni", "Minggu, 14 Juni", "Senin, 15 Juni", "Selasa, 15 Juni", "Rabu, 16 Juni"];
    const instance = tree.instance();
    instance.setDayDateStringArray(testDayDateArray);
    tree.update();
    instance.setComponents(testMatkulRuangArray);
    tree.update();
    expect(tree.find('FaChevronLeft').length).toBe(1);
    expect(tree.find('FaChevronRight').length).toBe(1);
    const spyOnClickChevronRight = jest.spyOn(SuccessPageComponent.prototype, 'onClickChevronRight');
    const spyOnClickChevronLeft = jest.spyOn(SuccessPageComponent.prototype, 'onClickChevronLeft');
    tree.find('FaChevronLeft').simulate('click');
    expect(spyOnClickChevronLeft).toHaveBeenCalled();
    tree.find('FaChevronRight').simulate('click');
    expect(spyOnClickChevronRight).toHaveBeenCalled();
    expect(instance.getDayDateStringArray().length/4).toBe(2);
    instance.onClickChevronRight()
    expect(instance.getNumberOfScrolls()).toBe(1)
    instance.onClickChevronRight()
    expect(instance.getNumberOfScrolls()).toBe(1)
    instance.onClickChevronLeft()
    expect(instance.getNumberOfScrolls()).toBe(0)
    instance.onClickChevronLeft();
    expect(instance.getNumberOfScrolls()).toBe(0)
    });
});