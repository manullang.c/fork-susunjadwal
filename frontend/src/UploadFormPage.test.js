import React from 'react';
import UploadForm from './components/upload_form/UploadForm.jsx';
import { render, fireEvent, screen} from '@testing-library/react';

describe("UploadForm GUI", () => {
    it("changes the label after inputting a file.", () => {
      const { getByText } = render(<UploadForm apiUrl="/" label="test" htmlFor="testing" />);
      const file = new File(['test'], 'test.xls', {
        type: 'application/vnd.ms-excel'
      });
      expect(screen.getByText("File untuk test")).toBeInTheDocument();
      fireEvent.change(
        screen.getByLabelText(/File untuk test/i), {
        target: { files: [file] }
      });
  
      expect(screen.getByText("test.xls")).toBeInTheDocument();
    });
  
    it("changes the label back after un-inputting a file.", () => {
      const { getByText } = render(<UploadForm apiUrl="/" label="test" htmlFor="testing" />);
      const file = new File(['test'], 'test.xls', {
        type: 'application/vnd.ms-excel'
      });
      fireEvent.change(
        screen.getByLabelText(/File untuk test/i), {
        target: { files: [file] }
      });
  
      const emptyFile = new File(['test'], 'ngeng.xls', {
        type: 'application/vnd.ms-excel'
      });
      fireEvent.change(
        screen.getByLabelText(/test.xls/i), {
        target: { files: [undefined] }
      });
  
      expect(screen.getByText("File untuk test")).toBeInTheDocument();
    });
  })