import React from 'react';
import { unmountComponentAtNode } from "react-dom";
import { shallow, configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import FailPageComponent from './components/fail_page/FailPageComponent.jsx';

configure({ adapter: new Adapter() });
let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

//from https://stackoverflow.com/questions/45478730/jest-react-testing-check-state-after-delay
describe('FailPageComponent Functions', () => {
    it('It Should Change State from 0, to 1 in 1500 ms',() => {
        jest.useFakeTimers();
        const tree = shallow(<FailPageComponent />);
        const instance = tree.instance();
        expect(instance.getStateCross()).toEqual(0);
        jest.advanceTimersByTime(1500);
        expect(instance.getStateCross()).toEqual(1);
    });
});
