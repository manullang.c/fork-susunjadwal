import React from 'react';
import { unmountComponentAtNode } from "react-dom";
import { shallow, configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import FileDropdown from './components/firebase_storage/FileDropdown';
import {storage} from './components/firebase_storage/FirebaseService'
import Button from 'react-bootstrap/Button'



configure({ adapter: new Adapter() });
let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

class testName {
    constructor(testName){
        this.name = testName;
    }
}

describe('FileDropdown functions', () =>{
    it('renders properly', () =>{
        const tree = shallow(
            <FileDropdown />
        );
        expect(tree).toMatchSnapshot();
    });
    it('setVisible function should set state to visible or hidden depending on input', () => {
        // We are testing the compoenent's state methods. Therefore there is no need for us to render the full component
        // and the subcomponents that it may have imported.
        const tree = shallow(
            <FileDropdown />
        );
        // Create a class instance out of a wrapper instance so that we can run class methods. We can't do that on a wrapper instance.
        const instance = tree.instance();
        // Then activate the method that we want to test...
        instance.setVisible("visible");
        // and assert equality with the value that we expect the state to be in after that method is run.
        expect(instance.state.dropdownVisibility).toBe("visible");
    })
    it('on menu button click when dropdownVisibility is visible the function activated should make it hidden', () => {
        // We are testing button functionality on the FileDropdown component. The buttons are from react bootstrap, and it is represented as a component.
        // Because of that, we want to render them. To do just that, we use the full rendering api by mounting the component that we want to test.
        let tree = mount(
            <FileDropdown  />
        );
        const instance = tree.instance();
        //I have no idea why you can't just set it through instance and update.
        tree.setState({dropdownVisibility: "visible"}) 
        // Since the state is now visible, when we click it should make it hidden
        tree.find('#menuButtonFiles').at(0).simulate('click');
        // Check whether the menubuttonfiles click simulation makes it hidden
        expect(tree.state().dropdownVisibility).toBe("hidden");
    });
});