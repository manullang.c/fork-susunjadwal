import React from 'react';
import { unmountComponentAtNode } from "react-dom";
import { shallow, configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import PageContainer from './components/PageContainer.jsx';
import SettingComponent from './components/settingPage/SettingComponent.jsx';
import RentangTanggalUjianComponent from './components/date_range_input/rentangTanggalUjianComponent';
import JumlahSesiPerHariComponent from './components/settingPage/JumlahSesiPerHariComponent.jsx';
import PilihTanggalUjianComponent from './components/settingPage/PilihTanggalUjianComponent.jsx';

configure({ adapter: new Adapter() });
let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

describe('SettingComponent', () => {
    it('SettingComponent should be defined', () => {
        expect(SettingComponent).toBeDefined();
    });
    it('SettingComponent should render correctly', () => {
        const tree = shallow(
        <SettingComponent name='setting test' />
        );
        expect(tree).toMatchSnapshot();
    });
});

describe('RentangTanggalUjianComponent', () => {
    it('RentangTanggalUjianComponent should be defined', () => {
        expect(RentangTanggalUjianComponent).toBeDefined();
    });
    it('RentangTanggalUjianComponent should render correctly', () => {
        const tree = shallow(
        <RentangTanggalUjianComponent name='setting test' />
        );
        expect(tree).toMatchSnapshot();
    });
});

describe('JumlahSesiPerHariComponent', () => {
    it('JumlahSesiPerHariComponent should be defined', () => {
        expect(JumlahSesiPerHariComponent).toBeDefined();
    });
    it('JumlahSesiPerHariComponent should render correctly', () => {
        const tree = shallow(
        <JumlahSesiPerHariComponent name='setting test' />
        );
        expect(tree).toMatchSnapshot();
    });
    it('JumlahSesiPerHariComponent button minus works', () => {
        const tree = shallow(
        <JumlahSesiPerHariComponent name='setting test' />
        );
        
    });
});

describe('PilihTanggalUjianComponent', () => {
    it('PilihTanggalUjianComponent should be defined', () => {
        expect(PilihTanggalUjianComponent).toBeDefined();
    });
    it('PilihTanggalUjianComponent should render correctly', () => {
        const tree = shallow(
            <PilihTanggalUjianComponent dateRange = {[new Date(2020, 4, 8), new Date(2020, 4, 10)]} visible = {true}/>
        );
        expect(tree).toMatchSnapshot();
    });
});

describe('PageContainer on state SettingPage', () => {
    it('Renders SettingComponent if current state is splash page', () => {
        const tree = shallow(
        <PageContainer name='PageContainer test' />
        );
        const instance = tree.instance();
        instance.changeStateToSetting();
        expect(tree.find(SettingComponent).length).toBe(1);
    });
    it('Does not render SettingComponent if current state is not splash page', () => {
        const tree = shallow(
        <PageContainer name='PageContainer test' />
        );
        tree.setState({ currentstate: "nothing_page" });
        expect(tree.find(SettingComponent).length).toBe(0);
    });
});


