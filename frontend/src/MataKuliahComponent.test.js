import React from 'react';
import { unmountComponentAtNode } from "react-dom";
import { shallow, configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import MataKuliahComponent from './components/success_page/MataKuliahComponent.jsx';

configure({ adapter: new Adapter() });
let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

describe('MataKuliahComponent Functions', () => {
    it('When the array supplied is ["_","_"] set all components to white and display both "_"s', () => {
        const suppliedArray = ["_","_"];
        const tree = mount(<MataKuliahComponent matkulKelasArray = {suppliedArray}/>)
        tree.update();
        expect(tree.find('.matkulBox').text()).toBe("_")
        expect(tree.find('.kelasBox').text()).toBe("_")
        expect(tree.find(".white").length).toBe(3);
    });
    it('When the array supplied is ["name1","name2"], display name1 and name2', () => {
        const suppliedArray = ["name1", "name2"];
        const tree = mount(<MataKuliahComponent matkulKelasArray = {suppliedArray}/>)
        console.log(tree.props.matkulKelasArray)
        expect(tree.find('.matkulBox').text()).toBe("name1")
        expect(tree.find('.kelasBox').text()).toBe("name2")
    });
});