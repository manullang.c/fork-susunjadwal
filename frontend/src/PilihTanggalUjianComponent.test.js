import React from 'react';
import { unmountComponentAtNode } from "react-dom";
import { shallow, configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import SettingComponent from './components/settingPage/SettingComponent.jsx';
import PilihTanggalUjianComponent from './components/settingPage/PilihTanggalUjianComponent.jsx';
import ActiveComponent from './components/settingPage/ActiveComponent.jsx'
import JumlahSesiPerHariComponent from './components/settingPage/JumlahSesiPerHariComponent.jsx';
import DisabledComponent from './components/settingPage/DisabledComponent.jsx';

configure({ adapter: new Adapter() });
let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

describe('SettingComponent', () => {
    it('should be able to set Date Range', () => {
        const tree = shallow(
            <SettingComponent name='setting test' />
        );
        const instance = tree.instance();
        instance.setDateRange(new Date(2020, 4, 6), new Date(2020, 4, 19));
        expect(instance.state.dateRange).toStrictEqual([new Date(2020, 4, 6), new Date(2020, 4, 19)])
    });
    it('PilihTanggalUjianComponent will change when date is modified', () => {
        const tree = mount(
            <SettingComponent name='setting test' />
        );
        const instance = tree.instance();
        instance.setDateRange(new Date(2020, 4, 6), new Date(2020, 4, 19));
        tree.update()
        expect(tree.find(PilihTanggalUjianComponent).find(ActiveComponent).length).toBe(14)
        instance.setDateRange(new Date(2020, 4, 7), new Date(2020, 4, 7));
        tree.update()
        expect(tree.find(PilihTanggalUjianComponent).find(ActiveComponent).length).toBe(1)
    })
    it('should be able to toggle visibility of PilihTanggalUjianComponent', () => {
        const tree = mount(
            <SettingComponent name='setting test' />
        );
        const instance = tree.instance();
        instance.setIsPilihTanggalUjianComponentVisible(false);
        expect(instance.state.isPilihTanggalUjianComponentVisible).toBeFalsy();
        instance.setIsPilihTanggalUjianComponentVisible(true);
        expect(instance.state.isPilihTanggalUjianComponentVisible).toBeTruthy();
    });
});

describe('ActiveComponent', () => {
    it('should be defined', () => {
        expect(ActiveComponent).toBeDefined();
    });
    it('should be rendered properly', () => {
        const mockFn = jest.fn();
        const tree = mount(
            <ActiveComponent date = {new Date()} dateEnabled = {mockFn}/>
        );
        expect(tree).toMatchSnapshot();
    });
    it('should call props.dateEnabled when clicked', () => {
        const mockFn = jest.fn();
        const tree = mount(
            <ActiveComponent date = {new Date()} dateEnabled = {mockFn} setCount = {() => {}}/>
        );
        tree.find('button').simulate('click'); //Runs a function which will call props.dateEnabled
        expect(mockFn).toHaveBeenCalled();
    });
    it('convertNumericMonthToMonthName should be able to convert month numbers called by Date.getMonth() to its respective month names', () => {
        const mockFn = jest.fn();
        const tree = mount(
            <ActiveComponent date = {new Date()} dateEnabled = {mockFn}/>
        );
        const instance = tree.instance();
        let mlist = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
        for (let i = 0; i < mlist.length; i++){
            let returnValue = instance.convertNumericMonthToMonthName(i);
            expect(returnValue).toBe(mlist[i])
        }
    });
    it('toggleDateEnabled should work', () => {
        const mockFn = jest.fn();
        const tree = mount(
            <ActiveComponent date = {new Date()} dateEnabled = {mockFn} setCount = {() => {}}/>
        );
        const instance = tree.instance();
        instance.toggleDateEnabled();
        expect(instance.state.ActiveComponentClass).toBe("ActiveComponent")
        instance.toggleDateEnabled();
        expect(instance.state.ActiveComponentClass).toBe("ActiveComponent disabled")
    });
})

describe('DisabledComponent', () => {
    it('should be defined', () => {
        expect(DisabledComponent).toBeDefined();
    });
    it('should be rendered properly', () => {
        const tree = shallow(
            <DisabledComponent date = {new Date()} dateEnabled = {jest.fn()}/>
        );
        expect(tree).toMatchSnapshot();
    });
    it('convertNumericMonthToMonthName should be able to convert month numbers called by Date.getMonth() to its respective month names', () => {
        const tree = shallow(
            <DisabledComponent date = {new Date()} dateEnabled = {jest.fn()}/>
        );
        const instance = tree.instance();
        let mlist = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
        for (let i = 0; i < mlist.length; i++){
            let returnValue = instance.convertNumericMonthToMonthName(i);
            expect(returnValue).toBe(mlist[i])
        }
    });
});

describe('PilihTanggalUjianComponent', () => {
    it('should be defined', () => {
        expect(PilihTanggalUjianComponent).toBeDefined();
    });
    it('should be rendered properly', () => {
        const tree = mount(
            <PilihTanggalUjianComponent dateRange = {[new Date(2020, 4, 8), new Date(2020, 4, 10)]} visible = {true}/>
        )
        expect(tree).toMatchSnapshot();
    });
    it('isDay should return true if day of date is the same', () => {
        const tree = mount(
            <PilihTanggalUjianComponent dateRange = {[new Date(2020, 4, 8), new Date(2020, 4, 10)]} visible = {true}/>
        )
        const instance = tree.instance();
        let randomDate = new Date(2020, 4, 8);
        let returnValue = instance.isDay(randomDate.getDay(), randomDate)
        expect(returnValue).toBeTruthy();
    });
    it('isDay should return false if day of date is not the same', () => {
        const tree = mount(
            <PilihTanggalUjianComponent dateRange = {[new Date(2020, 4, 8), new Date(2020, 4, 10)]} visible = {true}/>
        );
        const instance = tree.instance();
        let randomDate = new Date(2020, 4, 8);
        let returnValue = instance.isDay(2, randomDate)
        expect(returnValue).toBeFalsy();
    })
    it('isDateInDateRange should return true if date is inbetween dateRange', () => {
        const tree = mount(
            <PilihTanggalUjianComponent dateRange = {[new Date(2020, 4, 8), new Date(2020, 4, 10)]} visible = {true}/>
        );
        const instance = tree.instance();
        let returnValue = instance.isDateInDateRange(new Date(2020, 4, 9));
        expect(returnValue).toBeTruthy();
    });
    it('isDateInDateRange should return false if date is not inbetween dateRange', () => {
        const tree = mount(
            <PilihTanggalUjianComponent dateRange = {[new Date(2020, 4, 8), new Date(2020, 4, 10)]} visible = {true}/>
        );
        const instance = tree.instance();
        let returnValue = instance.isDateInDateRange(new Date(2020, 5, 20));
        expect(returnValue).toBeFalsy();
    });
    it('toggleDateEnabled should work', () => {
        const tree = mount(
            <PilihTanggalUjianComponent dateRange = {[new Date(2020, 4, 8), new Date(2020, 4, 10)]} visible = {true}/>
        );
        const instance = tree.instance();
        instance.toggleDateEnabled(new Date(2020, 4, 9)); // On
        let testArray = [new Date(2020, 4, 9)]
        expect(instance.state.dates_enabled.toString()).toBe(testArray.toString())
        instance.toggleDateEnabled(new Date(2020, 4, 9)); // Off
        expect(instance.state.dates_enabled.toString()).toBe([].toString())
        instance.toggleDateEnabled(new Date(2020, 4, 9)); // On
        instance.toggleDateEnabled(new Date(2020, 4, 10)); // On to explicitly cover else
        instance.toggleDateEnabled(new Date(2020, 4, 10)); // Off
        expect(instance.state.dates_enabled.toString()).toBe(testArray.toString())
    });
    it('getNearestDay should work', () => {
        const tree = mount(
            <PilihTanggalUjianComponent dateRange = {[new Date(2020, 4, 8), new Date(2020, 4, 10)]} visible = {true}/>
        );
        const instance = tree.instance();
        let randomDate = new Date(2020, 4, 9);
        let returnValue = instance.getNearestDay(1, randomDate, "forward");
        expect(returnValue.getDay()).toBe(1)
        returnValue = instance.getNearestDay(0, randomDate, "backward");
        expect(returnValue.getDay()).toBe(0)
        returnValue = instance.getNearestDay(0, randomDate, "nowhere"); //must explicitly cover else
        // tree.find(ActiveComponent).find('button').at(1).simulate('click');
    });
});
