import React from 'react';
import WelcomeComponent from './components/splashpage/WelcomeComponent.jsx';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';

configure({ adapter: new Adapter() });

describe('WelcomeComponent', () => {
    it('WelcomeComponent should be defined', () => {
      expect(WelcomeComponent).toBeDefined();
    });
    it('WelcomeComponent should render correctly', () => {
      const tree = shallow(
        <WelcomeComponent name='WelcomeComponent test' />
      );
      expect(tree).toMatchSnapshot();
    });
  });