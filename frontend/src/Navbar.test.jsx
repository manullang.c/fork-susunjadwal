import React from 'react';
import NavbarComponent from './components/common/NavbarComponent.jsx';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';

configure({ adapter: new Adapter() });

describe('Navbar', () => {
    it('Navbar should be defined', () => {
      expect(NavbarComponent).toBeDefined();
    });
    it('Navbar should render correctly', () => {
      const tree = shallow(
        <NavbarComponent name='navbar test' />
      );
      expect(tree).toMatchSnapshot();
    });
  });
  