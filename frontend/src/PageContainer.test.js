import React from 'react';
import PageContainer from './components/PageContainer.jsx';
import WelcomeComponent from './components/splashpage/WelcomeComponent'
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure, mount } from 'enzyme';
import TablePreview from './components/table_preview/TablePreview.jsx';

configure({ adapter: new Adapter() });

describe('PageContainer', () => {
    it('PageContainer should be defined', () => {
      expect(PageContainer).toBeDefined();
    });
    it('PageContainer should render correctly', () => {
      const tree = shallow(
        <PageContainer name='PageContainer test' />
      );
      expect(tree).toMatchSnapshot();
    });
    it('Renders WelcomeComponent if current state is splash page', () => {
      const tree = mount(
        <PageContainer name='PageContainer test' />
      );
      const instance = tree.instance()
      instance.changeStateToSplash();
      tree.update();
      expect(tree.find(WelcomeComponent).length).toBe(1);
    });
    it('Does not render WelcomeComponent if current state is not splash page', () => {
      const tree = mount(
        <PageContainer name='PageContainer test' />
      );
      tree.setState({ currentstate: "input_page" });
      expect(tree.find(WelcomeComponent).length).toBe(0);
    });
  });