import React from 'react';
import { unmountComponentAtNode } from "react-dom";
import { shallow, configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import LoadingSchedule from './components/loading_schedule/LoadingSchedule.jsx';

configure({ adapter: new Adapter() });
let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

//from https://stackoverflow.com/questions/45478730/jest-react-testing-check-state-after-delay
describe('LoadingSchedule Functions', () => {
    it('It Should Change State from 0, to 1, to 2, and back to 0 Every 1500 ms',() => {
        jest.useFakeTimers();
        const tree = shallow(<LoadingSchedule />);
        const instance = tree.instance();
        expect(instance.getHourglass()).toEqual(0);
        jest.advanceTimersByTime(1500);
        expect(instance.getHourglass()).toEqual(1);
        jest.advanceTimersByTime(1500);
        expect(instance.getHourglass()).toEqual(2);
        jest.advanceTimersByTime(1500);
        expect(instance.getHourglass()).toEqual(0);
    });
    it('switch case should go to default if case is not 0, 1 or 2', () => {
        const tree = mount(<LoadingSchedule />);
        const instance = tree.instance();
        instance.setHourglass(4);
        tree.update()
        expect(tree.find('#fail').length).toBe(1);
    });
});