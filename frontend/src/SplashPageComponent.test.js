import React from 'react';
import SplashWrapperComponent from './components/splashpage/SplashWrapperComponent.jsx';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure} from 'enzyme';

configure({ adapter: new Adapter() });

 describe('SplashWrapperComponent', () => {
    it('SplashWrapperComponent should be defined', () => {
      expect(SplashWrapperComponent).toBeDefined();
    });
    it('SplashWrapperComponent should render correctly', () => {
      const tree = shallow(
        <SplashWrapperComponent name='SplashWrapperComponent test' />
      );
      expect(tree).toMatchSnapshot();
    });
  });
