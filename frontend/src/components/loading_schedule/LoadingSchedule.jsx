import React from "react";
import { FaHourglassHalf, FaHourglassEnd } from 'react-icons/fa';
import "./LoadingSchedule.css"

const SIZE = "10vw"

export default class LoadingScheduleComponent extends React.Component{
    constructor(props){
        super(props);
        this.state={
            hourglass : 0,
        }
    }

    componentDidMount() {
        this.timeout = setInterval(() => {
            let hourglass = this.state.hourglass;
            hourglass++;
            if (hourglass > 2){
                hourglass = 0;
            }
            this.setHourglass(hourglass);
        }, 1500);
      }

    getHourglass(){
        return this.state.hourglass;
    }
    setHourglass(hourglass){
        this.setState({ hourglass: hourglass})
    }
    
    render() {
    let renderedComponent;
    switch(this.getHourglass()){
        case 0:
            renderedComponent = <FaHourglassEnd id = "animate" size ={SIZE}/>;
            break;
        case 1:
            renderedComponent = <FaHourglassHalf size={SIZE} />;
            break;
        case 2:
            renderedComponent = <FaHourglassEnd size={SIZE} />;
            break;
        default:
            renderedComponent = <div id="fail"></div>
            break;
    }
    return <div className="container bg-white min-h-75">
        <div id = "hour">{renderedComponent}</div>
        <div id = "text"><br/>Sistem sedang menyusun jadwal.<br/>Mohon tunggu sebentar.</div>
    </div>
    }
}

