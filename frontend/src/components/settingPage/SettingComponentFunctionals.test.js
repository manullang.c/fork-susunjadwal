import React from 'react';
import { render, fireEvent, screen, waitForElementToBeRemoved } from '@testing-library/react';

import SettingComponent from './SettingComponent';

let container = null;
beforeEach(() => {
    // setup a DOM element as a render target
    render(<SettingComponent
        apiUrl={''}
        setState={() => { }}
    />);
});

describe('range selector/date selector interaction', () => {
    it('functions normally', async () => {

        const date = screen.getAllByPlaceholderText(/tgl/i);
        const month = screen.getAllByDisplayValue(/bulan/i);
        const year = screen.getAllByPlaceholderText(/tahun/i);
        fireEvent.change(date[0], { target: { value: '21' } });
        fireEvent.change(date[1], { target: { value: '1' } });
        fireEvent.change(month[0], { target: { value: 'Mar' } });
        fireEvent.change(month[1], { target: { value: 'Apr' } });
        fireEvent.change(year[0], { target: { value: '2020' } });
        fireEvent.change(year[1], { target: { value: '2020' } });
        await screen.findByText("(12 hari)");

        // let plus = screen.findByText('+')
        // plus.click();

        screen.getByText("21").click();
        screen.getByText("22").click();

        screen.getByText("Jumlah hari ujian: 2 hari");
    });

    it('disappears when the date is changed to an invalid state', async () => {
        const date = screen.getAllByPlaceholderText(/tgl/i);
        const month = screen.getAllByDisplayValue(/bulan/i);
        const year = screen.getAllByPlaceholderText(/tahun/i);
        fireEvent.change(date[0], { target: { value: '21' } });
        fireEvent.change(date[1], { target: { value: '1' } });
        fireEvent.change(month[0], { target: { value: 'Mar' } });
        fireEvent.change(month[1], { target: { value: 'Apr' } });
        fireEvent.change(year[0], { target: { value: '2020' } });
        fireEvent.change(year[1], { target: { value: '2020' } });
        await screen.findByText("(12 hari)");

        screen.getByText("22");
        screen.getByText("21").click();
        fireEvent.change(year[0], { target: { value: '2022' } });

        await screen.findByText("Tanggal akhir harus datang sesudah tanggal awal.");
        screen.getByText("Jumlah hari ujian: 0 hari");


    });

    it('resets when you tried to change the date', async () => {
        const date = screen.getAllByPlaceholderText(/tgl/i);
        const month = screen.getAllByDisplayValue(/bulan/i);
        const year = screen.getAllByPlaceholderText(/tahun/i);
        fireEvent.change(date[0], { target: { value: '21' } });
        fireEvent.change(date[1], { target: { value: '6' } });
        fireEvent.change(month[0], { target: { value: 'Mar' } });
        fireEvent.change(month[1], { target: { value: 'Apr' } });
        fireEvent.change(year[0], { target: { value: '2020' } });
        fireEvent.change(year[1], { target: { value: '2020' } });
        await screen.findByText("(17 hari)");

        screen.getByText("22");
        screen.getByText("21").click();
        fireEvent.change(date[1], { target: { value: '2' } });

        await waitForElementToBeRemoved(() => screen.queryByText('6'));
        screen.getByText("Jumlah hari ujian: 0 hari");
    });
});
