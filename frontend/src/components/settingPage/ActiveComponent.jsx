import React from 'react';
import './ActiveComponent.css'

class ActiveComponent extends React.Component{
    constructor(props){
        super(props);
        this.state = { ActiveComponentClass: "ActiveComponent disabled"}
    }
    convertNumericMonthToMonthName(numericMonth){
        let mlist = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
        return mlist[numericMonth];
    }

    toggleDateEnabled(){
        if (this.state.ActiveComponentClass === "ActiveComponent"){
            this.setActiveComponentClass("ActiveComponent disabled");
            this.props.setCount(-1);
        }
        else{
            this.setActiveComponentClass("ActiveComponent");
            this.props.setCount(1);
        }
    }

    setActiveComponentClass(activeComponentClass){
        this.setState({ActiveComponentClass: activeComponentClass});
    }

    render() {
        return <>
        <button className = {this.state.ActiveComponentClass} onClick = {() => {this.props.dateEnabled(this.props.date); this.toggleDateEnabled()}}>
            <div>
                <div className = "datePortion">{this.props.date.getDate()}</div>
                <div className = "monthPortion">{this.convertNumericMonthToMonthName(this.props.date.getMonth())}</div>
            </div>
        </button>
        </>
    }
}

export default ActiveComponent;