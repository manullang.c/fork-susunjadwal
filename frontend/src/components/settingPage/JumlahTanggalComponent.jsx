import React from "react";

export default class JumlahTanggalComponent extends React.Component {
    shouldComponentUpdate() {
        return true;
    }
    render() {
        return <div>Jumlah hari ujian: {this.props.count} hari</div>
    }
}