import React from 'react';

class DisabledComponent extends React.Component{
    convertNumericMonthToMonthName(numericMonth){
        let mlist = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
        return mlist[numericMonth];
    }
    
    render() {
        return <>
        <button disabled>
            <div className = "DisabledComponent">
                <div className = "datePortion">{this.props.date.getDate()}</div>
                <div className = "monthPortion">{this.convertNumericMonthToMonthName(this.props.date.getMonth())}</div>
            </div>
        </button>
        </>
    }
}

export default DisabledComponent;