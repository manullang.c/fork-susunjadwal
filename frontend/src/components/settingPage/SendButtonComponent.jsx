import React from "react";

export default class SendButtonComponent extends React.Component {

    render() {
        return <div className="text-right p-3">
            <button
                className="btn btn-lg btn-primary"
                onClick={this.props.submit.bind(this)}
                style={{ display: this.props.visible ? "inline-block" : "none" }}
            >
                Berikutnya
        </button>
        </div>
    }
}