import React from 'react';
import RentangTanggalUjianComponent from '../date_range_input/rentangTanggalUjianComponent';
import JumlahSesiPerHariComponent from './JumlahSesiPerHariComponent.jsx';
import PilihTanggalUjianComponent from './PilihTanggalUjianComponent.jsx';
import SendButtonComponent from './SendButtonComponent'
import JumlahTanggalComponent from './JumlahTanggalComponent';

class SettingComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dateRange: [new Date(2020, 6, 2), new Date(2020, 6, 10)],
            isPilihTanggalUjianComponentVisible: false,
            dateCount: 0,
            sessionPerDay: 1
        };
        this.dateComponent = React.createRef();
        this.sessionPerDayComponent = React.createRef();
    }

    setDateRange(startDate, endDate) {
        this.setState({
            dateRange: [new Date(startDate.valueOf()), new Date(endDate.valueOf())]
        });
    }

    setSessionPerDay(spd) {
        this.setState({
            sessionPerDay: spd
        });
    }

    componentDidMount() {
        this.setDateRange(new Date(2020, 6, 2), new Date(2020, 6, 10))
    }

    setIsPilihTanggalUjianComponentVisible(isVisible) {
        this.setState({
            dateCount: 0,
            isPilihTanggalUjianComponentVisible: isVisible
        });
    }

    submit() {
        const dates = this.dateComponent.current.state.dates_enabled;
        this.props.setState(dates, this.state.sessionPerDay);
    }

    setCount(delta) {
        this.setState({
            dateCount: this.state.dateCount + delta
        })
    }

    render() {
        return <div className="container bg-white min-h-75">
            <RentangTanggalUjianComponent
                setVisibility={this.setIsPilihTanggalUjianComponentVisible.bind(this)}
                setDateRange={this.setDateRange.bind(this)}
            />
            <br />
            <JumlahSesiPerHariComponent
                update={this.setSessionPerDay.bind(this)}
            />
            <br />
            <PilihTanggalUjianComponent
                dateRange={this.state.dateRange}
                visible={this.state.isPilihTanggalUjianComponentVisible}
                setGeneratedDates={this.setGeneratedDates}
                ref={this.dateComponent}
                setCount={this.setCount.bind(this)} />
            <JumlahTanggalComponent
                count={this.state.dateCount}
            />
            <SendButtonComponent
                visible={this.state.isPilihTanggalUjianComponentVisible}
                submit={this.submit.bind(this)} />
        </div>
    }
}

export default SettingComponent;