import React from 'react';
import ActiveComponent from './ActiveComponent.jsx';
import DisabledComponent from './DisabledComponent.jsx';
import './PilihTanggalUjianComponent.css';


const MONDAY = 1;
const SUNDAY = 0;

var weekArray = [];
var componentArray = [];
var lines = 0;

class PilihTanggalUjianComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = { dates_enabled: [], components: [], enabledAmount: 0 }
        this.allow_updates = 0;
    }

    isDay(day, date) {
        if (date.getDay() === day) {
            return true;
        }
        return false;
    }

    isDateInDateRange(date) {
        if (date >= this.props.dateRange[0] && date <= this.props.dateRange[1]) {
            return true;
        }
        return false;
    }

    resetNeededComponents() {
        lines = 0;
        weekArray = [];
        componentArray = [];
    }

    shouldComponentUpdate(nextProps, nextState) {
        const nextDate = nextProps.dateRange;
        const nowDate = this.props.dateRange;
        if (this.props.visible === false) {
            return true
        } else if (nextProps.visible === false) {
            return true
        } else if (
            (nextDate[0].getTime() !== nowDate[0].getTime())
            || (nextDate[1].getTime() !== nowDate[1].getTime())) {
/* WARNING: THESE SECTION BELOW IS FULL OF MAGIC
   THE CREATOR ITSELF DOESN'T KNOW HOW THIS STUFF WORKS
   GIT GUD

   PLEASE REPLACE THIS PART OF THE CODE SOON.

   BEGIN MAGIC
*/
            this.allow_updates = 2;
            return true
        } else if (this.allow_updates > 0) {
            this.allow_updates -= 1;
            return true
/* END MAGIC
*/
        } else {
            return false
        }
    }

    generateDates(startDate, endDate) {
        this.resetNeededComponents();
        var nearestMonday = this.getNearestDay(MONDAY, startDate, "backward");
        var nearestSunday = this.getNearestDay(SUNDAY, endDate, "forward");
        var currentDate = nearestMonday;
        while (currentDate.valueOf() <= nearestSunday.valueOf()) {
            if (this.isDateInDateRange(currentDate)) {
                this.createActiveComponent(currentDate);
            }
            else {
                this.createDisabledComponent(currentDate);
            }
            if (this.isDay(SUNDAY, currentDate)) {
                this.makeNewLine();
            }
            var tempDate = new Date(currentDate.valueOf() + 86400000);
            currentDate = new Date(tempDate.valueOf());
        }
        this.setState({ dates_enabled: [], components: componentArray });
    }

    makeNewLine() {
        componentArray.push(weekArray);
        weekArray = [];
    }

    getNearestDay(day, cdate, direction) {
        var date = new Date(cdate.valueOf());
        if (direction === "forward") {
            while (date.getDay() !== day) {
                date = new Date(date.valueOf() + 86400000);
            }
        }
        else if (direction === "backward") {
            while (date.getDay() !== day) {
                date = new Date(date.valueOf() - 86400000);
            }
        }
        return date;
    }


    toggleDateEnabled(date) {
        var isDateInArray = false;
        for (var i = 0; i < this.state.dates_enabled.length; i++) {
            if (this.state.dates_enabled[i].valueOf() === date.valueOf()) {
                this.state.dates_enabled.splice(i, 1);
                isDateInArray = true;
            }
        }
        if (!isDateInArray) {
            this.state.dates_enabled.push(date);
        }
    }


    createActiveComponent(date) {
        weekArray.push(<ActiveComponent
            date={new Date(date)}
            dateEnabled={() => this.toggleDateEnabled(date)}
            setCount={this.props.setCount} />)
    }

    createDisabledComponent(date) {
        weekArray.push(<DisabledComponent date={new Date(date)} />)
    }

    construct() {
        var startDate = new Date(this.props.dateRange[0].valueOf());
        var endDate = new Date(this.props.dateRange[1].valueOf());
        this.generateDates(startDate, endDate);

    }

    componentDidMount(props) {
        this.construct();
    }

    componentDidUpdate(props) {
        if (props.dateRange.toString() !== this.props.dateRange.toString()) {
            this.construct();
        }
    }

    render() {
        return <>
            <h5>Pilih Tanggal Ujian:</h5>
            <table className="selection-table">
                <thead>
                    <tr>
                        <th>Senin</th>
                        <th>Selasa</th>
                        <th>Rabu</th>
                        <th>Kamis</th>
                        <th>Jumat</th>
                        <th>Sabtu</th>
                        <th>Minggu</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.components.map(
                        component =>
                            <tr key={lines++} style={{ display: this.props.visible ? "default" : "none" }}>
                                <td>{component[0]}</td>
                                <td>{component[1]}</td>
                                <td>{component[2]}</td>
                                <td>{component[3]}</td>
                                <td>{component[4]}</td>
                                <td>{component[5]}</td>
                                <td>{component[6]}</td>
                                <td>{component[7]}</td>
                            </tr>
                    )}
                </tbody>
            </table>

        </>
    }
}

export default PilihTanggalUjianComponent;