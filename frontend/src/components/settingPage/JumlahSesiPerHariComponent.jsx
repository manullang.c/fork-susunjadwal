import React from 'react';
import Button from 'react-bootstrap/Button';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import './JumlahSesiPerHariComponent.css';

class JumlahSesiPerHariComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = { jumlahSesi: 1 }
    }

    setJumlahSesi(jumlahSesi){
        if (jumlahSesi < 1){
            this.setState({jumlahSesi: 1})
        }
        else{
            this.setState({jumlahSesi: (jumlahSesi)})
        }
        this.props.update(jumlahSesi);
    }

    getJumlahSesi(){
        return this.state.jumlahSesi;
    }
    

    render() {
        return <>
        <InputGroup id = "jumlahSesiInputGroup">
            <h5 id="jumlahSesiText">Jumlah sesi per hari: </h5>
            <div className="jumlahSesiInputContainer">
                <Button id="minusButton" onClick={() => this.setJumlahSesi(this.state.jumlahSesi - 1)}>-</Button>
                <span className="buttonPadding"></span>
                <FormControl id = "jumlahSesiForm" disabled value ={this.state.jumlahSesi}/>
                <span className="buttonPadding"></span>
                <Button id="plusButton" onClick={() => this.setJumlahSesi(this.state.jumlahSesi + 1)}>+</Button>
            </div>
        </InputGroup>
        </>
    }
}

export default JumlahSesiPerHariComponent;