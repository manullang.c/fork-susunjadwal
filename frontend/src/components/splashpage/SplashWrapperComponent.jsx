//TODO Finish me
import React from 'react';
import SplashPageButton from './SplashPageButton';
import './SplashWrapperComponent.css';


class SplashWrapperComponent extends React.Component {
    render() {
        return (
            <div className="splashwrapper">
                <div className="bigtext">
                    Welcome to  
                    <p className="biggesttext"> Susun Jadwal <br />FEB</p>
                </div>
                <div className="splashPageButtonWrapper">
                    <SplashPageButton setState = {this.props.setState}/>
                </div>
            </div>
        )
    }
}

export default SplashWrapperComponent;
