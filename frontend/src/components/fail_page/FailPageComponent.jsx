import React from 'react';
import {FaRegTimesCircle} from 'react-icons/fa';
import './FailPageComponent.css'

const SIZE = "10vw";

export default class FailPageComponent extends React.Component {
    constructor(props){
        super(props);
        this.state = {stateCross: 0}
    }

    componentDidMount(){
        this.timeout = setInterval(() => {
            this.setStateCross(1);
        }, 1500);

    }

    setStateCross(stateCross){
        this.setState({stateCross: stateCross});
    }

    getStateCross(){
        return this.state.stateCross;
    }
    
    render() {
        let renderedComponent;
        if (this.getStateCross() === 1){
                renderedComponent = <><FaRegTimesCircle size={SIZE} id = "animate"/> </>
        } else{
                renderedComponent = <><FaRegTimesCircle size={SIZE} id = "opacityZero"/></>
        }
        return <>
        <div id = "fail">{renderedComponent}</div>
        <div id = "text">Jadwal tidak dapat disusun.<br />Silakan periksa kembali pengaturan anda</div>
        </>
    }
}