import React from 'react';
import { shallow, configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { render, fireEvent, screen } from '@testing-library/react';
import { unmountComponentAtNode } from "react-dom";

import RentangTanggalUjianComponent from './rentangTanggalUjianComponent';

let container = null;
beforeEach(() => {
    // setup a DOM element as a render target
    render(<RentangTanggalUjianComponent
        setVisibility={() => { }}
        setDateRange={() => { }} />);
});

describe('meta', () => {
    it('has the class defined', () => {
        expect(RentangTanggalUjianComponent).toBeDefined();
    });
});

describe('appearance', () => {
    it('has the label', () => {
        expect(screen.getByText("Rentang tanggal ujian:")).toBeInTheDocument();
    });

    it('has the date inputs', () => {
        const input = screen.getAllByPlaceholderText(/tgl/i);
        fireEvent.change(input[0], { target: { value: '21' } });
        fireEvent.change(input[1], { target: { value: '40' } });
        expect(input[0].value).toBe('21');
        expect(input[1].value).toBe('31');
    });

    it('has the year inputs', () => {
        const input = screen.getAllByPlaceholderText(/tahun/i);
        fireEvent.change(input[0], { target: { value: '2010' } });
        fireEvent.change(input[1], { target: { value: '2014' } });
        expect(input[0].value).toBe('2010');
        expect(input[1].value).toBe('2014');
    });
});

describe('error detection', () => {
    it('wont accept date ranges that is moving backwards', async () => {
        const date = screen.getAllByPlaceholderText(/tgl/i);
        const month = screen.getAllByDisplayValue(/bulan/i);
        const year = screen.getAllByPlaceholderText(/tahun/i);
        fireEvent.change(date[0], { target: { value: '21' } });
        fireEvent.change(date[1], { target: { value: '23' } });
        fireEvent.change(month[0], { target: { value: 'Mar' } });
        fireEvent.change(month[1], { target: { value: 'Feb' } });
        fireEvent.change(year[0], { target: { value: '2020' } });
        fireEvent.change(year[1], { target: { value: '2020' } });
        await screen.findByText("Tanggal akhir harus datang sesudah tanggal awal.");
    });

    it('wont accept date ranges that is over 4 weeks (4*7 days)', async () => {
        const date = screen.getAllByPlaceholderText(/tgl/i);
        const month = screen.getAllByDisplayValue(/bulan/i);
        const year = screen.getAllByPlaceholderText(/tahun/i);
        fireEvent.change(date[0], { target: { value: '21' } });
        fireEvent.change(date[1], { target: { value: '21' } });
        fireEvent.change(month[0], { target: { value: 'Mar' } });
        fireEvent.change(month[1], { target: { value: 'Apr' } });
        fireEvent.change(year[0], { target: { value: '2020' } });
        fireEvent.change(year[1], { target: { value: '2020' } });
        await screen.findByText("Selisih tanggal jangan lebih dari 4 minggu.");
    });

    it('wont accept incomplete data', async () => {
        const date = screen.getAllByPlaceholderText(/tgl/i);
        const month = screen.getAllByDisplayValue(/bulan/i);
        const year = screen.getAllByPlaceholderText(/tahun/i);
        fireEvent.change(date[0], { target: { value: '21' } });
        fireEvent.change(date[1], { target: { value: '21' } });
        fireEvent.change(month[0], { target: { value: 'Mar' } });
        fireEvent.change(month[1], { target: { value: 'Apr' } });
        fireEvent.change(year[0], { target: { value: '2020' } });
        fireEvent.change(year[1], { target: { value: '2020' } });
        await screen.findByText("Silakan lengkapi tanggal mulai dan selesai ujian.");
    });

    it('should otherwise detect correct dates correctly', async () => {
        const date = screen.getAllByPlaceholderText(/tgl/i);
        const month = screen.getAllByDisplayValue(/bulan/i);
        const year = screen.getAllByPlaceholderText(/tahun/i);
        fireEvent.change(date[0], { target: { value: '21' } });
        fireEvent.change(date[1], { target: { value: '1' } });
        fireEvent.change(month[0], { target: { value: 'Mar' } });
        fireEvent.change(month[1], { target: { value: 'Apr' } });
        fireEvent.change(year[0], { target: { value: '2020' } });
        fireEvent.change(year[1], { target: { value: '2020' } });
        await screen.findByText("(12 hari)");
    });
});