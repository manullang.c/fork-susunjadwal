import React from 'react';

export default class DateSelector extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            day: null,
            month: null,
            year: null
        };
        this.dateElement = React.createRef();
    }

    setStateAsync(state) {
        return new Promise((resolve) => {
          this.setState(state, resolve)
        });
    }

    async dateInputChange(evt) {
        let value = evt.target.value;
        value = value.replace(/\D/g, "");
        if (value !== "0") {
            value = value.replace(/^0+/, "");
        }
        value = value.slice(0, 2);
        const MAX_DAY = this.maxDays(this.state.month, this.state.year)
        if (parseInt(value) > MAX_DAY) {
            value = MAX_DAY;
        }
        if (parseInt(value) === 0) {
            value = "1";
        }
        evt.target.value = value;
        await this.setStateAsync({ day: parseInt(value) });
        this.props.callback();
    }

    async yearInputChange(evt) {
        let year = evt.target.value;
        year = year.replace(/\D/g, "");
        if (year !== "0") {
            year = year.replace(/^0+/, "");
        }
        year = year.slice(0, 4);

        evt.target.value = year;
        await this.setStateAsync({ year: parseInt(year) });
        this.fixDate(this.state.month, year);
    }

    async monthInputChange(evt) {
        const month = evt.target.value;
        await this.setStateAsync({ month: evt.target.value });
        this.fixDate(month, this.state.year);
    }

    maxDays(month, year) {
        const MAX_DATES = {
            Jan: 31,
            Feb: 28,
            Mar: 31,
            Apr: 30,
            May: 31,
            Jun: 30,
            Jul: 31,
            Aug: 31,
            Sep: 30,
            Oct: 31,
            Nov: 30,
            Dec: 31
        }
        if (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0) || (year === null))
            MAX_DATES.Feb = 29;
        if (month === null)
            return 31;

        return MAX_DATES[month];
    }

    async fixDate(month, year) {
        const MAX_DAY = this.maxDays(month, year);

        if (this.state.day > MAX_DAY) {
            await this.setStateAsync({day: MAX_DAY});
            this.dateElement.current.value = MAX_DAY;
        }

        this.props.callback();
    }

    render() {
        return <form action="" className="inline">
            <input type="text" name="tanggal" className="date-input"
                placeholder="Tgl"
                onChange={this.dateInputChange.bind(this)}
                ref={this.dateElement}
            />
            <select
                name="bulan" className="month-input"
                defaultValue="Bulan"
                onChange={this.monthInputChange.bind(this)}
            >
                <option value="null" hidden>Bulan</option>
                <option value="Jan">Jan</option>
                <option value="Feb">Feb</option>
                <option value="Mar">Mar</option>
                <option value="Apr">Apr</option>
                <option value="May">May</option>
                <option value="Jun">Jun</option>
                <option value="Jul">Jul</option>
                <option value="Aug">Aug</option>
                <option value="Sep">Sep</option>
                <option value="Oct">Oct</option>
                <option value="Nov">Nov</option>
                <option value="Dec">Dec</option>
            </select>
            <input type="text" name="tahun" className="year-input" placeholder="Tahun"
                onChange={this.yearInputChange.bind(this)}
            />
        </form>

    }
}
