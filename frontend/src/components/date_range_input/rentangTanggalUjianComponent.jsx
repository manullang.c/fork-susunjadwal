import React from 'react';
import DateSelector from './dateSelector'

import "./rentangTanggalUjian.scss"

export default class RentangTanggalUjianComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            message: "Silakan lengkapi tanggal mulai dan selesai ujian."
        }
        this.startElement = React.createRef();
        this.endElement = React.createRef();
    }
    processChildDates() {
        const startState = this.startElement.current.state;
        const startDate = new Date(`${startState.day} ${startState.month} ${startState.year}`);
        if (startState.year !== null && startState.month !== null && startState.day !== null) {
            startDate.setFullYear(startState.year);
        }
        const endState = this.endElement.current.state;
        const endDate = new Date(`${endState.day} ${endState.month} ${endState.year}`);
        if (endState.year !== null && endState.month !== null && endState.day !== null) {
            endDate.setFullYear(endState.year);
        }

        const deltaDay = (endDate.getTime() - startDate.getTime()) / (24 * 3600 * 1000)
        if (isNaN(endDate.getTime()) || isNaN(startDate.getTime())) {
            this.setState({ message: "Silakan lengkapi tanggal mulai dan selesai ujian." });
            this.props.setVisibility(false);
        } else if (deltaDay < 0) {
            this.setState({ message: "Tanggal akhir harus datang sesudah tanggal awal." });
            this.props.setVisibility(false);
        } else if (deltaDay > 4 * 7) {
            this.setState({ message: "Selisih tanggal jangan lebih dari 4 minggu." });
            this.props.setVisibility(false);
        } else {
            this.setState({ message: `(${deltaDay + 1} hari)` });
            this.props.setDateRange(startDate, endDate);
            this.props.setVisibility(true);
        }
    }

    render() {
        return <div>
            <h5>Rentang tanggal ujian:</h5>
            <div>
                <DateSelector
                    ref={this.startElement}
                    callback={this.processChildDates.bind(this)}
                />
                &nbsp;&mdash;&nbsp;
                <DateSelector
                    ref={this.endElement}
                    callback={this.processChildDates.bind(this)}
                />
            </div>
            <div >{this.state.message}</div>
        </div>
    }
}
