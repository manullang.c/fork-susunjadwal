import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';

import DateSelector from './dateSelector';

function getTanggal() {
    return screen.getByPlaceholderText(/tgl/i);
}

function getBulan() {
    return screen.getByDisplayValue(/bulan/i);
}

function getTahun() {
    return screen.getByPlaceholderText(/tahun/i);
}


let container = null;
beforeEach(() => {
    render(<DateSelector callback={() => {}}/>);
});



describe('meta', () => {
    it('has the class defined and its essential components', () => {
        expect(DateSelector).toBeDefined();
    });
});

describe('basic interface functionality', () => {
    it('has the date input behave as an input', () => {
        const input = getTanggal();
        fireEvent.change(input, { target: { value: '23' } });
        expect(input.value).toBe('23');
    });

    it('has the year input behave as an input', () => {
        const input = getTahun();
        fireEvent.change(input, { target: { value: '2020' } });
        expect(input.value).toBe('2020');
    });

    it('has the year input behave as a select input', () => {
        const input = getBulan();
        expect(input.value).toBe('null');
        fireEvent.change(input, { target: { value: 'Jan' } });
        expect(input.value).toBe('Jan');
        fireEvent.change(input, { target: { value: 'ABC' } });
        expect(input.value).toBe('null');
        fireEvent.change(input, { target: { value: 'Mar' } });
        expect(input.value).toBe('Mar');
    });
});

describe('input error fixing', () => {
    it('has date input input numbers only', () => {
        const input = getTanggal();
        fireEvent.change(input, { target: { value: '-lorem.1 ipsum9' } });
        expect(input.value).toBe('19');
    });

    it('has year input input numbers only', () => {
        const input = getTahun();
        fireEvent.change(input, { target: { value: '-lorem.2 ipsum0x1+9-' } });
        expect(input.value).toBe('2019');
    });

    it('limits the date input to 2 digits', () => {
        const input = getTanggal();
        fireEvent.change(input, { target: { value: '22222' } });
        expect(input.value).toBe('22');
    });

    it('limits the year input to 4 digits', () => {
        const input = getTahun();
        fireEvent.change(input, { target: { value: '202020' } });
        expect(input.value).toBe('2020');
    });

    it('fixes prepending zero for date', () => {
        const input = getTanggal();
        fireEvent.change(input, { target: { value: '014' } });
        expect(input.value).toBe('14');
    });
    it('fixes prepending zero for year, but not for year 0', () => {
        const input = getTahun();
        fireEvent.change(input, { target: { value: '014' } });
        expect(input.value).toBe('14');
        fireEvent.change(input, { target: { value: '0' } });
        expect(input.value).toBe('0');
    });
});

describe('date error fixing', () => {
    it('wont give dates above date 31', () => {
        const input = getTanggal();
        fireEvent.change(input, { target: { value: '40' } });
        expect(input.value).toBe('31');
    });

    it('wont give date 0', () => {
        const input = getTanggal();
        fireEvent.change(input, { target: { value: '0' } });
        expect(input.value).toBe('1');
    });

    it('handles month change', async () => {
        const date = getTanggal();
        const month = getBulan();
        fireEvent.change(date, { target: { value: '31' } });
        expect(date.value).toBe('31');
        fireEvent.change(month, { target: { value: 'Jan' } });
        expect(date.value).toBe('31');
        fireEvent.change(month, { target: { value: 'Apr' } });
        await screen.findByDisplayValue('Apr');
        expect(date.value).toBe('30');
    });

    it('handles leap year', async () => {
        const date = getTanggal();
        const month = getBulan();
        const year = getTahun();
        fireEvent.change(date, { target: { value: '31' } });
        expect(date.value).toBe('31');
        fireEvent.change(year, { target: { value: '2015' } });
        expect(year.value).toBe("2015");
        fireEvent.change(month, { target: { value: 'Feb' } });
        await screen.findByDisplayValue('Feb');
        expect(date.value).toBe('28');
    });
});