import React from 'react';
import  Navbar  from 'react-bootstrap/Navbar';
import './NavbarComponent.css';
import FileDropdown from '../firebase_storage/FileDropdown.jsx'

class NavbarComponent extends React.Component {
    render() {
        return (
            <div>
                <Navbar className= "navbar">
                    <Navbar.Brand className="navbar-hen">
                    Susun Jadwal FEB
                    </Navbar.Brand>
                </Navbar>
                <div className = "fileDropdown"><FileDropdown listUrl = {this.props.listUrl}/></div>
            </div>
        )
    }
}

export default NavbarComponent;
