import React from 'react';
import  Button  from 'react-bootstrap/Button';
import './ReturnToSplashComponent.css'


export default class ReturnToSplashComponent extends React.Component{

    returnToSplash(){
        this.props.returnToSplash();
    } 
    render(){
        return <>
        <Button className = "returnToSplash" onClick = {() => {this.returnToSplash()}}>{this.props.buttonText}</Button>
        </>
    }
}