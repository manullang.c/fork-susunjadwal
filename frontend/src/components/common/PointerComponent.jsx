import React from 'react';
import './PointerComponent.css';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

class PointerComponent extends React.Component {
    render() {
        return <Container>
            <Row>
                <Col className={this.props.result === true ? 'inactive' : ''}>
                <button className="btn-circle" disabled>&nbsp;&nbsp;1&nbsp;&nbsp;</button>
                <h3 className="text-brown">Input dan Konfigurasi</h3>
                </Col>
                <Col className={this.props.result === false ? 'inactive' : ''}>
                <button className="btn-circle" disabled>&nbsp;&nbsp;2&nbsp;&nbsp;</button>
                <h3 className="text-brown">Output</h3>
                </Col>
            </Row>
        </Container>
    }
}

export default PointerComponent;