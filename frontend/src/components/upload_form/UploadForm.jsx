import React from 'react';
import axios from 'axios';


function toTitleCase(str) {
    return str.replace(
        /\w\S*/g,
        function (txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        }
    );
}

/**
 * The single upload form class. This contains the ne
 * 
 * Make two instances of this class: one for
 * Jadwal Kuliah, one for Berkas Ujian.
 * 
 * Please extend this class if you wish (or just copy the processfile
 * method.)
 * 
 * @example
 * <UploadForm label="Jadwal kuliah" apiUrl="backend.com/api" name="jadwal"/>
 * @
 */
class UploadForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            /** The currently selected file. */
            selectedFile: null,

            /** Message for error prompt. Optional. */
            errorMessage: "",

            /** (Bootstrap) class of input form. */
            inputFormClass: "custom-file-input",

            /** (Bootstrap) label of the custom file. */
            customFileLabel: "File untuk " + this.props.label
        }
        this.fileFormat = [
            'application/vnd.ms-excel',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        ];
        this.onInputChangeHandler = this.onInputChangeHandler.bind(this);
    }


    /**
     * Call this method to process file, sending it to API url.
     * 
     * To backend user, the request will have a JSON attribute like
     * {"file": (file data here)}.
     */
    async sendFile() {
        const data = new FormData();
        data.append('file', this.state.selectedFile);
        await axios.post(this.props.apiUrl, data);
        return;
    }


    /** This method shall be invoked when you select your file. */
    changeFile(file) {
        this.setState({
            selectedFile: file,
        });
        let label = "File untuk " + this.props.label;
        if (file !== undefined) {
            label = file.name;
        }

        this.setState({
            customFileLabel: label,
        });
    }

    setError(message) {
        this.setState({ errorMessage: message });
        if (message !== "") {
            this.setState({ inputFormClass: "custom-file-input is-invalid" })
        } else {
            this.setState({ inputFormClass: "custom-file-input" })
        }
    }

    /**
     * This method will validate the function call.
     */
    validateFile() {
        let file = this.state.selectedFile

        if (file == null) {
            this.setError('File tidak boleh kosong.');
            return false;
        }
        if (this.fileFormat.every(type => file.type !== type)) {
            this.setError('Format file harus .xls atau .xlsx.');
            return false;
        }

        return true;
    }


    /**
     * This method is called when user changed its input files.
     * 
     * @param {Event} event The event arguments when this method is called.
     */
    onInputChangeHandler(event) {
        this.changeFile(event.target.files[0]);
        this.setError("");
    }

    /**
     * Implementation of the render function.
     * 
     * @returns {JSX.Element} a JSX renderable element.
     */
    render() {
        return (
            <form>
                <h5>{toTitleCase(this.props.label)}</h5>
                <div className="custom-file">
                    <input
                        type="file"
                        className={this.state.inputFormClass}
                        name={this.props.htmlFor}
                        id={this.props.htmlFor}
                        onChange={this.onInputChangeHandler}
                        accept={this.fileFormat.join(", ")}
                        lang="id" />

                    <label className="custom-file-label" htmlFor={this.props.htmlFor} >
                        {this.state.customFileLabel}
                    </label>

                    <div className="invalid-tooltip">
                        {this.state.errorMessage}
                    </div>
                </div>
            </form>
        );
    }
}

export default UploadForm;