/** This error is thrown when the file is empty. */
class FileEmptyError extends Error { }

/** This error is thrown when the file format is unsupported. */
class InvalidFileError extends Error { }

export { FileEmptyError, InvalidFileError };