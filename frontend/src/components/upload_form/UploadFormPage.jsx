import React from 'react';
import UploadForm from './UploadForm';

/**
 * The entire form page. Consists of the two forms and a send button.
 */
class UploadFormPage extends React.Component {
    constructor(props) {
        super(props);
        this.schedule = React.createRef();
        this.rooms = React.createRef();
        this.submit = this.submit.bind(this);
        this.state = {
            buttonText: "Berikutnya"
        }
    }

    async submit() {
        this.setState({buttonText: "Mengupload..."})
        const schedIsValid = this.schedule.current.validateFile();
        const roomsIsValid = this.rooms.current.validateFile();

        if (schedIsValid && roomsIsValid) {
            await this.schedule.current.sendFile();            
            await this.rooms.current.sendFile();
            this.props.setState();
        }
    }
    render() {
        return <div className="container bg-white h-75">
            <UploadForm
                apiUrl={this.props.schedulePostUrl}
                htmlFor="jadwal"
                label="jadwal kuliah"
                ref={this.schedule}
            />
            <br/>
            <UploadForm
                apiUrl={this.props.roomsPostUrl}
                htmlFor="ruang"
                label="ruang ujian"
                ref={this.rooms}
            />

            <div className="text-right p-3">
                <button className="btn btn-lg btn-primary" onClick={this.submit}>
                    {this.state.buttonText}
                </button>
            </div>
        </div>
    }
}

export default UploadFormPage;