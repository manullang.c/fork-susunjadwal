//Requires Firebase to be installed using the command 'yarn add firebase'   
import firebase from "firebase/app";
import "firebase/storage";

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyBMT3WLPZ6r64f4cnios6223xlJnX8tjV8",
  authDomain: "trial-project-88bcb.firebaseapp.com",
  databaseURL: "https://trial-project-88bcb.firebaseio.com",
  projectId: "trial-project-88bcb",
  storageBucket: "trial-project-88bcb.appspot.com",
  messagingSenderId: "451141671906",
  appId: "1:451141671906:web:331eb3e6d452e26b4d37bb"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const storage = firebase.storage();

export { firebase, storage }; 