import React from 'react';
import Button from 'react-bootstrap/Button';
import {storage} from './FirebaseService.js' 
import './FileDropdown.css'
import { FaBars } from 'react-icons/fa';


export default class SampleUploadButton extends React.Component{
    constructor(props){ 
        super(props);
        this.state = {
            xlsx: null,
            url: "",
            progress: 0,
            listOfFiles : [],
            totalStorageOccupied: 0,
            dropdownVisibility : "hidden"
        };
        
    }

    /*
    //Sample Upload Code
    handleUpload = async () => {
        //Insert Xlsx File Here. This text file is a placeholder.
        const xlsx = new File(["foo"], "foo.txt", {
            type: "text/plain",
          });
        const uploadTask = storage.ref(`${xlsx.name}`).put(xlsx)
        uploadTask.on("state_changed", snapshot => {
                const progress = Math.round(
                    (snapshot.bytesTransferred / snapshot.totalBytes) * 100
                );
            },
            error => {
                console.log(error);
            },
        );
    }*/

    getJSON(url){
        return new Promise(function (resolve, reject){
            var xhr = new XMLHttpRequest();
            xhr.open('GET', url, true);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status === 200){
                    resolve(xhr.response)
                } else {
                    reject(status);
                }
            };
            xhr.send();
        })
    }
    
    // from https://web.archive.org/web/20200502183806/http://janhesters.com/updater-functions-in-setstate/
    // async from https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function
    getList = async () => {
        var returnedListOfFile = [];
        var totalSizeOfFile = 0;
        var results = await this.getJSON(this.props.listUrl);
        //Get this function call to give the results synchronously so that the result will be set to data before running the for loop.
        for (let i = 0; i < results.items.length; i++){
            const eachMetadata = results.items[i]
            returnedListOfFile.push(eachMetadata);
            totalSizeOfFile = totalSizeOfFile + eachMetadata.size;
        }
        this.setState({ listOfFiles: returnedListOfFile, totalStorageOccupied: totalSizeOfFile})
        this.setVisible("visible")
    }

    deleteFile = async (name) => {
        console.log(name)
        this.getJSON(this.props.listUrl + "/delete/" + name)
        await this.setVisible("hidden")
    }

    setVisible = (visibility) => {
        this.setState({ dropdownVisibility: visibility})
    }

    downloadFile = async (name) => {
        this.getJSON(this.props.listUrl + "/download/" + name)
    }

    getListOfFiles() {
        return this.state.listOfFiles;
    }

    render(){
        let listOfFiles = this.getListOfFiles();
        return <>
        <div className = "navbarButton"><Button id = "menuButtonFiles" onClick = {this.state.dropdownVisibility === "visible"? () => this.setVisible("hidden") : this.getList }> <FaBars size={"70px"} /></Button></div>
        <div className = "dropdown" style={{visibility: this.state.dropdownVisibility}}>
            {listOfFiles.map(
            eachFile => <div key = {eachFile.name} id = {eachFile.name}><div>{eachFile.name}</div>
                            <div className = "dropdownButtons"><div><Button className = "downloadButton" onClick = {() => this.downloadFile(eachFile.name)} download> Download </Button>
                            <Button className = "deleteButton" onClick = {() => this.deleteFile(eachFile.name)}> Delete </Button></div></div><hr/></div>
        )}
        <div>{this.state.totalStorageOccupied + 0} / {1073741824 * 5} of storage occupied.</div></div>
        </>
    }
}