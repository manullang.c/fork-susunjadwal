import React from 'react';
import DayDateHeaderComponent from './DayDateHeaderComponent.jsx';
import MataKuliahComponent from './MataKuliahComponent.jsx';
import './SuccessPageComponent.css';
import {FaChevronRight, FaChevronLeft} from 'react-icons/fa';
import UnduhJadwalButtonComponent from './UnduhJadwalButtonComponent.jsx';

export default class SuccessPageComponent extends React.Component {
    constructor(props){
        super(props);
        this.state = {numberOfScrolls: 0, dayDateString: ["Rabu, 10 Juni", "Kamis, 11 Juni", "Jumat, 12 Juni", "Sabtu, 13 Juni", "Minggu, 14 Juni", "Senin, 15 Juni", "Selasa, 15 Juni", "Rabu, 16 Juni"],
         components: [[0,[["matkul", "ruang"],["matkul", "ruang"],["matkul", "ruang"]], [["matkul", "ruang"],["matkul", "ruang"],["matkul", "ruang"]], [["matkul", "ruang"],["matkul", "ruang"],["matkul", "ruang"]], [["matkul", "ruang"],["matkul", "ruang"],["matkul", "ruang"]], 
         0, [["matkul", "ruang"],["matkul", "ruang"],["matkul", "ruang"]], [["matkul", "ruang"],["matkul", "ruang"],["_", "_"]], [["matkul", "ruang"],["matkul", "ruang"],["matkul", "ruang"]], [["matkul", "ruang"],["matkul", "ruang"],["matkul", "ruang"]]]] };
    }

    onClickChevronRight(){
        if(this.getNumberOfScrolls() + 1 < this.getDayDateStringArray().length/4){
            this.setNumberOfScrolls(this.getNumberOfScrolls() + 1);
        }
    }

    onClickChevronLeft(){
        if(this.getNumberOfScrolls() > 0){
            this.setNumberOfScrolls(this.getNumberOfScrolls() - 1);
        }
    }

    getDayDateStringArray(){
        return this.state.dayDateString;
    }

    getDayDateString(index){
        return this.state.dayDateString[index];
    }

    setDayDateStringArray(arrayOfDayDateString){
        this.setState({dayDateString : arrayOfDayDateString});
    }

    setComponents(componentArray){
        this.setState({components: componentArray});
    }

    getComponents(){
        return this.state.components;
    }

    getNumberOfScrolls() {
        return this.state.numberOfScrolls;
    }

    setNumberOfScrolls(numberOfScrolls){
        this.setState({numberOfScrolls: numberOfScrolls});
    }
    
    render(){
        let lines = 0;
        return <>
            <div className = "horiz-stack">
            <FaChevronLeft onClick = {() => {this.onClickChevronLeft()}} className = {this.getNumberOfScrolls() > 0 ? "chevron active" : "chevron disabled"} />
            <table className="selection-table">
                <thead>
                    <tr>
                        <th className = "sesi-numbers"></th>
                        <th><DayDateHeaderComponent dayDateString = {this.getDayDateString(0 + 4 * this.getNumberOfScrolls())}/></th>
                        <th><DayDateHeaderComponent dayDateString = {this.getDayDateString(1 + 4 * this.getNumberOfScrolls())}/></th>
                        <th><DayDateHeaderComponent dayDateString = {this.getDayDateString(2 + 4 * this.getNumberOfScrolls())}/></th>
                        <th><DayDateHeaderComponent dayDateString = {this.getDayDateString(3 + 4 * this.getNumberOfScrolls())}/></th>
                    </tr>
                </thead>
                <tbody>
                {this.getComponents().map(
                    eachComponentGroups =>
                        <tr key={lines++}>
                            <td><div className = "number">{eachComponentGroups[0 + 5 * this.getNumberOfScrolls()]}</div></td>
                            <td>{eachComponentGroups[1 + 5 * this.getNumberOfScrolls()].map(
                                eachComponent =>
                                <div className="td-matkul" key = {eachComponent + "1" + lines++}><MataKuliahComponent matkulKelasArray = {eachComponent} /></div>
                                )} 
                            </td>
                            <td>{eachComponentGroups[2 + 5 * this.getNumberOfScrolls()].map(
                                eachComponent =>
                                <div className="td-matkul" key = {eachComponent + "2" + lines++}><MataKuliahComponent matkulKelasArray = {eachComponent} /></div>
                                )} 
                            </td>
                            <td>{eachComponentGroups[3 + 5 * this.getNumberOfScrolls()].map(
                                eachComponent =>
                                <div className="td-matkul" key = {eachComponent + "3" + lines++}><MataKuliahComponent matkulKelasArray = {eachComponent} /></div>)} 
                            </td>
                            <td>{eachComponentGroups[4 + 5 * this.getNumberOfScrolls()].map(
                                eachComponent =>
                                <div className="td-matkul" key = {eachComponent + "4" + lines++}><MataKuliahComponent matkulKelasArray = {eachComponent} /></div>
                                )} 
                            </td>
                        </tr>
                    )}
                </tbody>
            </table>
            <FaChevronRight onClick = {() => {this.onClickChevronRight()}} className = {this.getNumberOfScrolls() + 1 < this.getDayDateStringArray().length/4 ? "chevron active" : "chevron disabled"} />
            </div>
            <div className = "center"><UnduhJadwalButtonComponent downloadUrl={this.props.downloadUrl}/></div>
            </>
    }
}