import React from 'react';
import './MataKuliahComponent.css'

export default class MataKuliahComponent extends React.Component {

    render(){
        let matkulBoxClassName = "matkulBox";
        let kelasBoxClassName = "kelasBox"
        let mataKuliahComponentClassName = "mataKuliahComponent"
        if (this.props.matkulKelasArray[0] === "_" && this.props.matkulKelasArray[1] === "_"){
            matkulBoxClassName = "matkulBox white"
            kelasBoxClassName = "kelasBox white"
            mataKuliahComponentClassName = "mataKuliahComponent white"
        }
        
    return <>
        <div className = {mataKuliahComponentClassName}>
            <div className={matkulBoxClassName}>{this.props.matkulKelasArray[0]}</div>
            <div className={kelasBoxClassName}>{this.props.matkulKelasArray[1]}</div>
        </div>
    </>
    }
}