import React from 'react';
import './DayDateHeaderComponent.css';

export default class DayDateHeaderComponent extends React.Component {
    render(){
        return <div className = "dayDateHeaderComponent">{this.props.dayDateString}</div>
    }
}