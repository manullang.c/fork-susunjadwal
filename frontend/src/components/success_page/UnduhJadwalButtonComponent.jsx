import React from 'react';
import {FaFileExcel} from 'react-icons/fa'
import Button from 'react-bootstrap/Button'
import './UnduhJadwalButtonComponent.css'

export default class UnduhJadwalButtonComponent extends React.Component {
    constructor(props){
        super(props);
        this.state = {filename: "Karena masalah teknis, jadwal ujian belum dapat ditampilkan " +
    "di sini. Untuk sementara, silakan download xlsx."};
    }

    setFilename(filename) {
        this.setState({filename: filename});
    }

    getFilename(){
        return this.state.filename;
    }

    //TODO Connect this with the backend in order for the program to generate the file
    downloadFile(){
        return;
    }

    render() {
        return <>
        <div><span className = "unduhFileicon"><FaFileExcel /></span><span className = "unduhFilename">{this.getFilename()}</span></div>
        <div className = "unduhDiv" > <a className = "unduhJadwal btn btn-primary" href={this.props.downloadUrl}>Unduh Jadwal</a></div>
        </>
    }
}