import React from 'react';

import NavbarComponent from './common/NavbarComponent.jsx';
import WelcomeComponent from './splashpage/WelcomeComponent.jsx';
import UploadFormPage from './upload_form/UploadFormPage.jsx';
import TablePreview from './table_preview/TablePreview.jsx';
import SettingComponent from './settingPage/SettingComponent.jsx';
import SuccessPageComponent from './success_page/SuccessPageComponent.jsx';
import LoadingSchedule from './loading_schedule/LoadingSchedule.jsx';

import './common/bootstrap.scss'
import PointerComponent from './common/PointerComponent.jsx';
import FailPageComponent from './fail_page/FailPageComponent.jsx';
import ReturnToSplashComponent from './common/ReturnToSplashComponent.jsx';

class PageContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentstate: "splash_page",
      sessionPerDay: 1,
      cachedDates: [],
      result: false,
    }
    this.changeStateToInput.bind(this)
  }

  changeStateToSplash() {
    this.setState({
      currentstate: 'splash_page',
    })
  }

  changeStateToInput() {
    this.setState({
      currentstate: 'input_page',
      result: false,
    })
  }

  async changeStateToResult(dates, spd) {
    await new Promise((resolve) => {
      this.setState({
        cachedDates: dates,
        sessionPerDay: spd
      }, resolve)
    });
    this.setState({
      currentstate: 'result_page',
      result: true,
    });

  }

  changeStateToSetting() {
    this.setState({
      currentstate: 'setting_page',
      result: false,
    })
  }

  render() {
    let pointerComponent;
    let renderedComponent;
    if (this.state.currentstate === "splash_page") {
      renderedComponent = <WelcomeComponent setState={this.changeStateToInput.bind(this)} />
    } else {
      pointerComponent = <PointerComponent result={this.state.result}/>
      if (this.state.currentstate === "input_page") {
        renderedComponent = <UploadFormPage
          setState={this.changeStateToSetting.bind(this)}
          schedulePostUrl={this.props.schedulePostUrl}
          roomsPostUrl={this.props.roomsPostUrl}
        />
      } else if (this.state.currentstate === "result_page") {
        renderedComponent = <TablePreview
          changeStateToSplash = {() => {this.changeStateToSplash()}}
          apiUrl={this.props.tableGetUrl}
          downloadUrl={this.props.downloadUrl}
          dates={this.state.cachedDates}
          sessionPerDay={this.state.sessionPerDay}
          />
      }
      else if (this.state.currentstate === "setting_page") {
        renderedComponent = <SettingComponent
          apiUrl={this.props.DateSendUrl}
          setState={this.changeStateToResult.bind(this)}
        />
      }
    }

    return (
      <>
        <link href='https://fonts.googleapis.com/css?family=Oldenburg' rel='stylesheet'></link>
        <link href='https://fonts.googleapis.com/css?family=Rubik' rel='stylesheet'></link>
        <NavbarComponent listUrl = {this.props.listUrl}/>
        <span style={{ fontFamily: "Rubik, serif" }}>
          {pointerComponent}
          {renderedComponent}
        </span>
      </>
    );
  }
}

export default PageContainer;