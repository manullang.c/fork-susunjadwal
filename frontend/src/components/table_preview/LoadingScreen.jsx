import React from 'react';

class LoadingScreen extends React.Component {
    render() {
        return <div className="text-center">
            <h2 >Loading...</h2>
            <div className="spinner-border" ></div>
        </div>
    }
}

export default LoadingScreen;