import React from 'react';
import axios from 'axios';
import LoadingScreen from '../loading_schedule/LoadingSchedule';
import FailingPage from '../fail_page/FailPageComponent';
import SuccessPage from '../success_page/SuccessPageComponent';
import ReturnToSplashComponent from '../common/ReturnToSplashComponent';

// class Table extends React.Component {

//     constructor(props) {
//         super(props);
//         this.getHeader = this.getHeader.bind(this);
//         this.getRowsData = this.getRowsData.bind(this);
//         this.getKeys = this.getKeys.bind(this);
//     }

//     getKeys() {
//         return Object.keys(this.props.data[0]);
//     }

//     getHeader() {
//         var keys = this.getKeys();
//         return keys.map((key, index) => {
//             return <th key={key}>{key}</th>
//         })
//     }

//     getRowsData() {
//         var items = this.props.data;
//         var keys = this.getKeys();
//         return items.map((row, index) => {
//             return <tr key={keys}><RenderRow
//                 key={index}
//                 data={row}
//                 keys={keys}
//             /></tr>
//         })
//     }


//     render() {
//         return <div>
//             <table className="table table-responsive">
//                 <thead>
//                     <tr>{this.getHeader()}</tr>
//                 </thead>
//                 <tbody>
//                     {this.getRowsData()}
//                 </tbody>
//             </table>
//         </div>;
//     }
// }

// function RenderRow(props) {
//     return props.keys.map((key, index) => {
//         return <td key={key}>{props.data[key]}</td>
//     })
// }

/**
 * The output table viewer class. This class will wait with a loading
 * screen first until the table data has got.
 */
class TablePreview extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tableContent: <LoadingScreen />,
            dateListContent: []
        }
    }
    componentDidMount() {
        const dateListContent = [];
        this.props.dates.forEach(date => {
            dateListContent.push(<li key={date.toString()}>{date.toString()}</li>)
        });
        this.setState({
            dateListContent: dateListContent
        });

        console.log(this.props.sessionPerDay);

        axios.post(this.props.apiUrl, {
            dates: this.props.dates,
            sessionPerDay: this.props.sessionPerDay
        }
        ).then(res => {
            if (!res.data.success) {
                this.setState({
                    tableContent: <> <FailingPage changeStateToSplash = {() => {this.props.changeStateToSplash()}}/>
                    <div align = "center"><ReturnToSplashComponent returnToSplash = {() => {this.props.changeStateToSplash()}} buttonText = "Atur Ulang Jadwal"/></div></>
                })
            } else {
                this.setState({
                    tableContent:
                    // <pre>{JSON.stringify(res.data.result, null, 2)}</pre>
                    <>    
                        <SuccessPage
                            // dayDateString={res.data.result[0]}
                            // components={res.data.result[1]}
                            downloadUrl={this.props.downloadUrl}
                        />
                        <div align = "center"><ReturnToSplashComponent returnToSplash = {() => {this.props.changeStateToSplash()}} buttonText = "Kembali ke laman utama"/></div></>
                })
            }
        });
    }
    render() {
        return <div className="container bg-white min-h-75">
            {this.state.tableContent}
        </div>
    }
}

export default TablePreview;