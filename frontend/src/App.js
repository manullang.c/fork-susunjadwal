import React from 'react';
import './App.css';
import PageContainer from './components/PageContainer.jsx';

const BACKEND_URL = "http://localhost:3000"
//const BACKEND_URL = "https://susun-jadwal-backend-staging.herokuapp.com"

function App() {
  return (
    <PageContainer
      schedulePostUrl={BACKEND_URL + "/upload/schedule"}
      roomsPostUrl={BACKEND_URL + "/upload/room"}
      tableGetUrl={BACKEND_URL + "/get_table"}
      downloadUrl={BACKEND_URL + "/download"}
      listUrl={BACKEND_URL + "/list_file"}
    />
  );
}

export default App;
