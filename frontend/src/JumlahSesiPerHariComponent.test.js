import React from 'react';
import { unmountComponentAtNode } from "react-dom";
import { shallow, configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import JumlahSesiPerHariComponent from './components/settingPage/JumlahSesiPerHariComponent.jsx';


configure({ adapter: new Adapter() });
let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

describe('JumlahSesiPerHariComponent Functions', () => {
    it('Plus Button should add jumlahSesi', () => {
      const tree = shallow(
        <JumlahSesiPerHariComponent update={() => {}}/>
        );
        tree.find('#plusButton').simulate('click');
        const instance = tree.instance();
        expect(instance.getJumlahSesi()).toBe(2);
    });
    it('Minus Button should subtract the jumlahSesi', () => {
      const tree = shallow(
        <JumlahSesiPerHariComponent update={() => {}}/>
        );
        tree.find('#plusButton').simulate('click');
        tree.find('#plusButton').simulate('click');
        tree.find('#minusButton').simulate('click');
        const instance = tree.instance();
        expect(instance.getJumlahSesi()).toBe(2);
    });
    it('Minus Button should not subtract the jumlahSesi if jumlahSesi is one', () => {
      const tree = shallow(
        <JumlahSesiPerHariComponent update={() => {}} />
        );
        tree.find('#minusButton').simulate('click');
        const instance = tree.instance();
        expect(instance.getJumlahSesi()).toBe(1);
    });
});




