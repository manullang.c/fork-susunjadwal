import React from 'react';
import SplashPageButton from './components/splashpage/SplashPageButton.jsx';
import PageContainer from './components/PageContainer';
import WelcomeComponent from './components/splashpage/WelcomeComponent'
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure, mount } from 'enzyme';

configure({ adapter: new Adapter() });

describe('SplashPageButton', () => {
    it('should be defined', () => {
      expect(SplashPageButton).toBeDefined();
    });
    it('Splash Page Button should render correctly', () => {
      const tree = shallow(
        <SplashPageButton name='splash page button test' />
      );
      expect(tree).toMatchSnapshot();
    });
    it('should not render elements on splash page if currentstate is not splash_page', () => {
      const tree = mount(
        <PageContainer />
      );
      tree.find('button').first().simulate('click');
      expect(tree.contains(<WelcomeComponent />)).toBe(false);
    });
  });