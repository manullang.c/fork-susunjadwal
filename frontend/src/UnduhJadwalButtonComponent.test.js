import React from 'react';
import { unmountComponentAtNode } from "react-dom";
import { shallow, configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import UnduhJadwalButtonComponent from './components/success_page/UnduhJadwalButtonComponent.jsx';

configure({ adapter: new Adapter() });
let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

describe('UnduhJadwalButtonComponent functions', () => {
    it('changes the filename when set', () => {
        const tree = mount(
            <UnduhJadwalButtonComponent />
        );
        const instance = tree.instance();
        instance.setFilename("testFilename");
        expect(instance.getFilename()).toBe("testFilename");
    });

    //from https://stackoverflow.com/questions/51296502/how-to-test-a-function-is-called-in-react-jest
    it('runs the download function when clicked', () => {
      const FakeFun = jest.spyOn(UnduhJadwalButtonComponent.prototype, 'downloadFile');
      const tree = mount(
        <UnduhJadwalButtonComponent />
      );
      tree.find('a').simulate('click');
      tree.update();
      // expect(FakeFun).toHaveBeenCalled();
    });
});