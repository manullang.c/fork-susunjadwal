import React from 'react';
import UploadForm from './components/upload_form/UploadForm.jsx';
import UploadFormPage from './components/upload_form/UploadFormPage.jsx';
import App from './App';
import axios from 'axios';
import { render, fireEvent, screen, waitForElement } from '@testing-library/react';

describe("UploadForm GUI", () => {
    it("changes the label after inputting a file.", () => {
      const { getByText } = render(<UploadForm apiUrl="/" label="test" htmlFor="testing" />);
      const file = new File(['test'], 'test.xls', {
        type: 'application/vnd.ms-excel'
      });
      expect(screen.getByText("File untuk test")).toBeInTheDocument();
      fireEvent.change(
        screen.getByLabelText(/File untuk test/i), {
        target: { files: [file] }
      });
  
      expect(screen.getByText("test.xls")).toBeInTheDocument();
    });
  
    it("changes the label back after un-inputting a file.", () => {
      const { getByText } = render(<UploadForm apiUrl="/" label="test" htmlFor="testing" />);
      const file = new File(['test'], 'test.xls', {
        type: 'application/vnd.ms-excel'
      });
      fireEvent.change(
        screen.getByLabelText(/File untuk test/i), {
        target: { files: [file] }
      });
  
      const emptyFile = new File(['test'], 'ngeng.xls', {
        type: 'application/vnd.ms-excel'
      });
      fireEvent.change(
        screen.getByLabelText(/test.xls/i), {
        target: { files: [undefined] }
      });
  
      expect(screen.getByText("File untuk test")).toBeInTheDocument();
    });
  })

  describe("SendFile", () => {
    it("won't send a non-xlsx file.", async () => {
      const { getAllByText } = render(<UploadFormPage apiUrl="/" />);
  
      const file = new File(['test'], 'test.txt', {
        type: 'text/txt'
      });
      fireEvent.change(
        screen.getByLabelText(/File untuk jadwal kuliah/i), {
        target: { files: [file] }
      });
      fireEvent.click(screen.getByText(/berikutnya/i));
      await waitForElement(() => getAllByText('Format file harus .xls atau .xlsx.'));
    });
  
    it("won't send an empty file.", async () => {
      const { getByText, getAllByText } = render(<UploadFormPage apiUrl="/" />);
      fireEvent.click(screen.getByText(/berikutnya/i));
      await waitForElement(() => getAllByText('File tidak boleh kosong.'));
    });
  
    it("will send a file succesfully", async () => {
      const { getAllByText } = render(<App apiUrl="/" />);
  
      fireEvent.click(screen.getByText(/Buat Jadwal Ujian Baru/i));
  
  
      const file = new File(['test'], 'test.xls', {
        type: 'application/vnd.ms-excel'
      });
      axios.post.mockImplementationOnce(() => Promise.resolve({}));
      fireEvent.change(
        screen.getByLabelText(/File untuk jadwal kuliah/i), {
        target: { files: [file] }
      });
      axios.post.mockImplementationOnce(() => Promise.resolve({}));
      fireEvent.change(
        screen.getByLabelText(/File untuk ruang ujian/i), {
        target: { files: [file] }
      });
  
      fireEvent.click(screen.getByText(/berikutnya/i));
  
      await waitForElement(() => getAllByText('Rentang tanggal ujian:'));
  
      const date = screen.getAllByPlaceholderText(/tgl/i);
      const month = screen.getAllByDisplayValue(/bulan/i);
      const year = screen.getAllByPlaceholderText(/tahun/i);
      fireEvent.change(date[0], { target: { value: '21' } });
      fireEvent.change(date[1], { target: { value: '1' } });
      fireEvent.change(month[0], { target: { value: 'Mar' } });
      fireEvent.change(month[1], { target: { value: 'Apr' } });
      fireEvent.change(year[0], { target: { value: '2020' } });
      fireEvent.change(year[1], { target: { value: '2020' } });
      await screen.findByText("(12 hari)");
  
      screen.getByText("21").click();
      screen.getByText("22").click();
  
      screen.getByText("Jumlah hari ujian: 2 hari");
  
      const tableData = {
        data: {
          success: true
        }
      }
      axios.post.mockImplementationOnce(() => Promise.resolve(tableData));
      fireEvent.click(screen.getByText(/berikutnya/i));
  
      await waitForElement(() => getAllByText(/unduh jadwal/i));
    })
  
    it("shows the failing page", async () => {
      const { getAllByText } = render(<App apiUrl="/" />);
  
      fireEvent.click(screen.getByText(/Buat Jadwal Ujian Baru/i));
  
  
      const file = new File(['test'], 'test.xls', {
        type: 'application/vnd.ms-excel'
      });
      axios.post.mockImplementationOnce(() => Promise.resolve({}));
      fireEvent.change(
        screen.getByLabelText(/File untuk jadwal kuliah/i), {
        target: { files: [file] }
      });
      axios.post.mockImplementationOnce(() => Promise.resolve({}));
      fireEvent.change(
        screen.getByLabelText(/File untuk ruang ujian/i), {
        target: { files: [file] }
      });
  
      fireEvent.click(screen.getByText(/berikutnya/i));
  
      await waitForElement(() => getAllByText('Rentang tanggal ujian:'));
  
      const date = screen.getAllByPlaceholderText(/tgl/i);
      const month = screen.getAllByDisplayValue(/bulan/i);
      const year = screen.getAllByPlaceholderText(/tahun/i);
      fireEvent.change(date[0], { target: { value: '21' } });
      fireEvent.change(date[1], { target: { value: '1' } });
      fireEvent.change(month[0], { target: { value: 'Mar' } });
      fireEvent.change(month[1], { target: { value: 'Apr' } });
      fireEvent.change(year[0], { target: { value: '2020' } });
      fireEvent.change(year[1], { target: { value: '2020' } });
      await screen.findByText("(12 hari)");
  
      screen.getByText("21").click();
      screen.getByText("22").click();
  
      screen.getByText("Jumlah hari ujian: 2 hari");
  
      const tableData = {
        data: {
          success: false
        }
      }
      axios.post.mockImplementationOnce(() => Promise.resolve(tableData));
      fireEvent.click(screen.getByText(/berikutnya/i));
  
      await waitForElement(() => getAllByText(/.*jadwal tidak dapat disusun.*/i));
    })
  })
  